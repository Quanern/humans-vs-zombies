package com.noroff.no.server.controller;

import com.noroff.no.server.models.*;
import com.noroff.no.server.repositories.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.net.URI;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("api/v1/game/{gameId}/kill")
@RestController
public class KillController {
    private final KillRepository killRepository;
    private final GameRepository gameRepository;
    private final PlayerRepository playerRepository;
    private final SquadRepository squadRepository;
    private final SquadMemberRepository squadMemberRepository;

    public KillController(KillRepository killRepository, GameRepository gameRepository, PlayerRepository playerRepository, SquadRepository squadRepository, SquadMemberRepository squadMemberRepository) {
        this.killRepository = killRepository;
        this.gameRepository = gameRepository;
        this.playerRepository = playerRepository;
        this.squadRepository = squadRepository;
        this.squadMemberRepository = squadMemberRepository;
    }

    /**
     * This method is used to get all the kills in a game
     *
     * @param gameId is the id of the game the user wants to get the kills from
     * @return 200 if the fetch is successful.
     */
    @Operation(summary = "Get all kills in game.", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Kill.class))),
            @ApiResponse(responseCode = "204", description = "No kills yet in game"),
            @ApiResponse(responseCode = "404", description = "Game not found")})
    @GetMapping()
    @RolesAllowed({"Admin", "User"})
    public ResponseEntity<?> getAllKillsInGame(@Parameter(description = "Id of the game to get the kills from")
                                               @PathVariable(value = "gameId") Long gameId) {

        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game with id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }
        List<Kill> kills = killRepository.findAllByGameGameId(gameId);
        if (kills.size() == 0) {
            String message = String.format("There are no kills yet in game (%d)", gameId);
            return new ResponseEntity<>(message, HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok()
                .header("message", "chats fetched successful!")
                .body(kills);
    }

    /**
     * This method is used to get a specific kill in a game
     *
     * @param gameId is the game Id where the kill is
     * @param killId is the id of the kill to fetch.
     * @return 200 if the kill is fetched successfully
     * 400 if there is mismatch between path variables and data in the database.
     * 404 if the game or kill could not be found.
     */
    @Operation(summary = "Get a kill in a specific game.", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Kill.class))),
            @ApiResponse(responseCode = "400", description = "Bad request. " +
                    "Kill with given id does not belong to given game id. " +
                    "Game is completed. "),
            @ApiResponse(responseCode = "404", description = "Game or kill not found"),
    })
    @GetMapping("/{killId}")
    @RolesAllowed({"Admin", "User"})
    public ResponseEntity<?> getOneKill(@Parameter(description = "Id of the game where the kill is")
                                        @PathVariable(value = "gameId") Long gameId,
                                        @Parameter(description = "Id of the kill to get")
                                        @PathVariable(value = "killId") Long killId) {
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game id (%d) is not found", gameId);
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        } else if (!killRepository.existsById(killId)) {
            String message = String.format("Kill id (%d) is not found", killId);
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        } else if (!killRepository.existsByGameGameIdAndKillId(gameId, killId)) {
            String message = String.format("Kill id (%d) is not in game (%d)", killId, gameId);
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        Kill kill = killRepository.getOne(killId);
        return ResponseEntity.ok()
                .header("message", "Kill fetched successful")
                .body(kill);
    }

    /**
     * This method is used to create a kill
     *
     * @param kill   the kill object to be created
     * @param gameId the id of the game the kill happens in
     * @return 201 if the kill is created successfully
     * 400 if there is mismatch between path variables and data in the database.
     * 404 if the game, kill, killer or victim could not be found.
     */
    @Operation(summary = "Create a kill in a specific game.", security = @SecurityRequirement(name = "Bearer-token"))
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Kill to be posted.",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Kill.class))},
            required = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "POST request successful"),
            @ApiResponse(responseCode = "400", description = "Bad Request. Body does not contain victim and killer. " +
                    "Game already completed. " +
                    "Game id in path does not match game id for killer or victim. " +
                    "Killer is not a zombie or victim is not a human. " +
                    "Killer is trying to kill himself."),
            @ApiResponse(responseCode = "404", description = "Game, killer or victim not found"),
    })
    @PostMapping()
    @RolesAllowed({"Admin", "User"})
    public ResponseEntity<?> createKill(@RequestBody Kill kill,
                                        @Parameter(description = "Id of the game to create a kill in")
                                        @PathVariable(value = "gameId") Long gameId) {
        // Checks if the victim's biteCode and killer's Id is in the body.
        if (kill.getKiller().getPlayerId() == null || kill.getVictim().getBiteCode() == null) {
            String message = "There must be a valid killer id and a valid victim bite code.";
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        // Checks if the game exists
        if (!gameRepository.existsById(gameId)) {
            String message = "Game could not be found";
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        Game game = gameRepository.getOne(gameId);
        if (game.getGameState().getStateId().equals(3L)) {
            String message = String.format("Game (%d) is already completed.", gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        // Checks if the player exists
        if (!playerRepository.existsById(kill.getKiller().getPlayerId())) {
            String message = String.format("Player id (%d) could not be found", kill.getKiller().getPlayerId());
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        // Checks if the victim's biteCode exists in the game
        if (!playerRepository.existsByBiteCodeAndGameGameId(kill.getVictim().getBiteCode(), gameId)) {
            String message = String.format("Could not find a player with bite code (%s) and game id (%d)",
                    kill.getVictim().getBiteCode(), gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        // Fetches the player and victim entities.
        Player killer = playerRepository.getOne(kill.getKiller().getPlayerId());
        Player victim = playerRepository.findByBiteCodeAndGameGameId(kill.getVictim().getBiteCode(), gameId);
        // Sets the game if the kill body doesn't have a gameId
        if (kill.getGame() == null || kill.getGame().getGameId() == null) {
            kill.setGame(new Game(gameId));
        }
        // Checks if the killer is in the same game as the path Id
        if (!kill.getGame().getGameId().equals(gameId)) {
            String message = String.format("Game id (%d) in path does not match game id (%d) for killer",
                    gameId, kill.getGame().getGameId());
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        // Checks if killer and victim is in the same game
        else if (!killer.getGame().getGameId().equals(victim.getGame().getGameId())) {
            String message = String.format("Game id (%d) for killer does not match game id (%d) for victim",
                    killer.getGame().getGameId(), victim.getGame().getGameId());
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        // Checks if the killer is a human
        else if (killer.getHuman()) {
            String message = "Killer is not a zombie";
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        // Checks if the victim is a human
        else if (!victim.getHuman()) {
            String message = "Victims must be human";
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        // Checks if the killer is killing himself.
        else if (killer.getPlayerId().equals(victim.getPlayerId())) {
            String message = "You can not kill yourself";
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        } else {
            // Kills the victim and turn them into zombie
            victim.setHuman(false);
            playerRepository.save(victim);

        }
        kill.setTimeOfDeath(new Date());
        kill.setVictim(victim);
        Kill returnKill = killRepository.save(kill);
        // If victim is in an active squad
        if (victim.squadMemberGetter() != null) {
            // Gets the squadMember for the victim
            SquadMember squadMember = squadMemberRepository.getOne(victim.squadMemberGetter());
            squadMember.setActive(false);
            squadMember.setKilled(true);
            squadMemberRepository.save(squadMember);
            // Increase the killed count in the squad.
            Squad squad = squadRepository.getOne(squadMember.getSquad().getSquadId());
            squad.setDeceased(squad.getDeceased() + 1);
            squadRepository.save(squad);
        }
        URI location = URI.create(String.format("/game/%d/kill/%d", gameId, returnKill.getKillId()));
        return ResponseEntity.created(location)
                .header("message", "Kill added successful!")
                .body(returnKill);
    }

    /**
     * This method is used to update a kill in a game
     *
     * @param kill   is the kill object to update to
     * @param gameId is the id of the game where the kill is
     * @param killId is the id of the kill to be updated.
     * @return ResponseEntity with an error message or the updated kill.
     */
    @Operation(summary = "Update kill in a game.", security = @SecurityRequirement(name = "Bearer-token"))
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Kill to be updated.",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Kill.class))},
            required = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "PUT request successful. Expected changes:" +
                    "Description of the kill (story). "),
            @ApiResponse(responseCode = "400", description = "Bad Request. Game already completed. " +
                    "Game id in path does not match game id in body. " +
                    "Another player than the killer is trying to edit the kill. " +
                    "Kill id in path does not match kill id in body."),
            @ApiResponse(responseCode = "404", description = "Game, kill, killer or victim not found"),
    })
    @PutMapping("/{killId}")
    @RolesAllowed({"Admin", "User"})
    public ResponseEntity<?> updateKill(@RequestBody Kill kill,
                                        @Parameter(description = "Id of game where the kill to update is")
                                        @PathVariable(value = "gameId") Long gameId,
                                        @Parameter(description = "Id of the kill to update")
                                        @PathVariable(value = "killId") Long killId,
                                        @AuthenticationPrincipal Jwt principal) {
        // If the kill has no game or gameId in the body
        if (kill.getGame() == null || kill.getGame().getGameId() == null) {
            kill.setGame(new Game(gameId));
        }
        // Otherwise check if the game in the path and the body matches.
        else if (!kill.getGame().getGameId().equals(gameId)) {
            String message = String.format("Game id (%d) in path does not match the game id (%d) in body",
                    gameId, killId);
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        Player player = playerRepository.getOne(kill.getKiller().getPlayerId());
        if (!principal.getClaimAsStringList("roles").contains("Admin")) {
            if (!principal.getClaimAsString("preferred_username").equals(player.getUser().getUsername())) {
                String message = "You can not edit another person's kill";
                return ResponseEntity.badRequest()
                        .header("message", message)
                        .body(message);
            }
        }
        // If the kill is not set in the body, set the path's killId to the body.
        if (kill.getKillId() == null) {
            kill.setKillId(killId);
        }
        // Otherwise check if the killId in the kill and in the path matches
        else if (!kill.getKillId().equals(killId)) {
            String message = String.format("Kill id (%d) in path does not match kill id (%d) in body",
                    killId, kill.getKillId());
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        // Checks if game exists
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game id (%d) could not be found",
                    gameId);
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        Game game = gameRepository.getOne(gameId);
        if (game.getGameState().getStateId().equals(3L)) {
            String message = String.format("Game (%d) is already completed.", gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        // Checks if a kill exists
        else if (!killRepository.existsById(killId)) {
            String message = String.format("Kill id (%d) could not be found", killId);
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        // Gets the kill in the database and update the story to the new story.
        Kill oldKill = killRepository.getOne(killId);
        oldKill.setStory(kill.getStory());
        Kill returnKill = killRepository.save(oldKill);
        return ResponseEntity.ok()
                .header("message", "Edit successful!")
                .body(returnKill);
    }

    /**
     * This method is used to delete a kill from a game and revert what happened to the victim.
     *
     * @param gameId is the id where the kill is.
     * @param killId is the id of the kill to delete.
     * @return 200 if delete is successful. 404 if kill or game could not be found.
     * 400 if the kill is in another game.
     */
    @Operation(summary = "Delete a kill in a game.", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "DELETE request successful. Expected changes: " +
                    "Removal of the kill from the database."),
            @ApiResponse(responseCode = "400", description = "Bad Request. Game already completed." +
                    "Given kill id does not belong in given game id."),
            @ApiResponse(responseCode = "404", description = "Game or kill not found"),
    })
    @DeleteMapping("/{killId}")
    @RolesAllowed("Admin")
    public ResponseEntity<?> deleteKill(@Parameter(description = "Id of the game where the kill to delete is")
                                        @PathVariable(value = "gameId") Long gameId,
                                        @Parameter(description = "Id of the kill to delete")
                                        @PathVariable(value = "killId") Long killId) {
        // Check if game exists
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        Game game = gameRepository.getOne(gameId);
        if (game.getGameState().getStateId().equals(3L)) {
            String message = String.format("Game (%d) is already completed.", gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        // Check if game exists
        else if (!killRepository.existsById(killId)) {
            String message = String.format("Kill id (%d) could not be found", killId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        // Check if kill exists in the game
        else if (!killRepository.existsByGameGameIdAndKillId(gameId, killId)) {
            String message = String.format("Kill id (%d) could not be found in game (%d)", killId, gameId);
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        Kill kill = killRepository.getOne(killId);
        Player victim = playerRepository.getOne(kill.getVictim().getPlayerId());
        // Changes the victim of the kill back to human
        victim.setHuman(true);
        playerRepository.save(victim);
        killRepository.delete(kill);
        return new ResponseEntity<>("Delete successful!", HttpStatus.OK);
    }


}
