package com.noroff.no.server.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "squad_members")
public class SquadMember {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "squad_member_id")
    private Long squadMemberId;

    @Column(name = "rank")
    private String rank;

    @ManyToOne
    @JoinColumn(name = "game_id", nullable = false)
    private Game game;

    @ManyToOne
    @JoinColumn(name = "squad_id")
    private Squad squad;

    @ManyToOne
    @JoinColumn(name = "player_id", nullable = false)
    private Player player;

    @OneToMany(mappedBy = "squadMember", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<SquadCheckin> squadCheckins;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name="is_killed")
    private Boolean isKilled;

    public SquadMember(Player player) {
        this.player = player;
    }

    @JsonGetter("game")
    public Long gameGetter(){
        if(game == null){
            return null;
        }
        return game.getGameId();
    }

    @JsonGetter("squad")
    public Long squadGetter(){
        if(squad == null){
            return null;
        }
        return squad.getSquadId();
    }
    @JsonGetter("player")
    public Long playerGetter(){
        if(player == null){
            return null;
        }
        return player.getPlayerId();
    }

    @JsonGetter("squadCheckins")
    public List<Long> squadCheckinGetter(){
        List<Long> list = new ArrayList<>();
        if (squadCheckins != null) {
            for (SquadCheckin squadCheckin: squadCheckins) {
                list.add(squadCheckin.getSquadCheckinId());
            }
            return list;
        }
        return null;
    }

    @JsonGetter("username")
    public String usernameGetter(){
        if(player.getUser() == null || player.getUser().getUsername() == null){
            return null;
        }
        return player.getUser().getUsername();
    }

    @JsonSetter("squad")
    public void squadSetter(Long squadId){
        this.setSquad(new Squad(squadId));
    }

    @JsonSetter("game")
    public void gameSetter(Long gameId){
        this.setGame(new Game(gameId));
    }

    public SquadMember() {}

    public SquadMember(String rank, Game game, Squad squad, Player player, Boolean isActive, Boolean isKilled) {
        this.rank = rank;
        this.game = game;
        this.squad = squad;
        this.player = player;
        this.isActive = isActive;
        this.isKilled = isKilled;
    }

    public SquadMember(Long squadMemberId) {
        this.squadMemberId = squadMemberId;
    }

    public Long getSquadMemberId() {
        return squadMemberId;
    }

    public void setSquadMemberId(Long squadMemberId) {
        this.squadMemberId = squadMemberId;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game gameId) {
        this.game = gameId;
    }

    public Squad getSquad() {
        return squad;
    }

    public void setSquad(Squad squadId) {
        this.squad = squadId;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player playerId) {
        this.player = playerId;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Set<SquadCheckin> getSquadCheckins() {
        return squadCheckins;
    }

    public void setSquadCheckins(Set<SquadCheckin> squadCheckins) {
        this.squadCheckins = squadCheckins;
    }

    public Boolean getKilled() {
        return isKilled;
    }

    public void setKilled(Boolean killed) {
        isKilled = killed;
    }
}

