package com.noroff.no.server.controller;

import com.noroff.no.server.models.*;
import com.noroff.no.server.repositories.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.net.URI;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("api/v1/game/{gameId}/squad/{squadId}")
@RestController
public class SquadMemberController {
    private final SquadMemberRepository squadMemberRepository;
    private final SquadRepository squadRepository;
    private final PlayerRepository playerRepository;
    private final GameRepository gameRepository;
    private final SquadCheckinRepository squadCheckinRepository;

    public SquadMemberController(SquadMemberRepository squadMemberRepository, SquadRepository squadRepository, PlayerRepository playerRepository, GameRepository gameRepository, SquadCheckinRepository squadCheckinRepository) {
        this.squadMemberRepository = squadMemberRepository;
        this.squadRepository = squadRepository;
        this.playerRepository = playerRepository;
        this.gameRepository = gameRepository;
        this.squadCheckinRepository = squadCheckinRepository;
    }

    /**
     * This method is used to create a squadMember and join a squad.
     *
     * @param gameId  is the id of the game where player wants to join a squad.
     * @param squadId the squad to join
     * @param player  is the player that wants to create a new squad member.
     * @return the created squad member.
     */
    @Operation(summary = "Create a squadMember.", security = @SecurityRequirement(name = "Bearer-token"))
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "SquadMember to be created.",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Player.class))},
            required = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "POST request successful"),
            @ApiResponse(responseCode = "400", description = "Bad Request. " +
                    "There must be a valid player in the request " +
                    "Given squad does not belong in given game. " +
                    "Player does not belong in given game. " +
                    "Game already completed. " +
                    "Player can not join enemy squad. " +
                    "Player is already in squad. "),
            @ApiResponse(responseCode = "404", description = "Game, squad or player not found"),
    })
    @PostMapping("/join")
    @RolesAllowed({"User", "Admin"})
    public ResponseEntity<?> createSquadMember(@Parameter(description = "Id of the game the squad is")
                                               @PathVariable(value = "gameId") Long gameId,
                                               @Parameter(description = "Id of the squad the squadMember will join")
                                               @PathVariable(value = "squadId") Long squadId,
                                               @RequestBody Player player) {
        SquadMember squadMember = new SquadMember();
        if (player == null || player.getPlayerId() == null) {
            String message = "There must be a valid player id in the request.";
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        squadMember.setSquad(new Squad(squadId));
        if (player.getGame() == null || player.getGame().getGameId() == null) {
            squadMember.setGame(new Game(gameId));
        } else {
            squadMember.setGame(player.getGame());
        }
        if (!playerRepository.existsById(player.getPlayerId())) {
            String message = String.format("Player id (%d) could not be found", squadMember.getPlayer().getPlayerId());
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        if (!squadRepository.existsById(squadId)) {
            String message = String.format("Squad id (%d) could not be found", squadId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        } else if (!squadRepository.existsSquadBySquadIdAndGameGameId(squadId, gameId)) {
            String message = String.format("Squad id(%d) does not belong in game (%d)", squadId, gameId);
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }

        Game game = gameRepository.getOne(gameId);
        if (game.getGameState().getStateId().equals(3L)) {
            String message = String.format("Game (%d) is already completed.", gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }

        player = playerRepository.getOne(player.getPlayerId());
        Squad squad = squadRepository.getOne(squadId);

        if (!squadMember.getGame().getGameId().equals(gameId)) {
            String message = String.format("Game id (%d) in path does not match game id (%d) for checkin",
                    gameId, squadMember.getGame().getGameId());
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        } else if (!playerRepository.existsByGameGameIdAndPlayerId(gameId, player.getPlayerId())) {
            String message = String.format("Player id (%d) does not belong in game (%d)",
                    player.getPlayerId(), gameId);
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        } else if (player.getHuman() && !squad.getHuman() || !player.getHuman() && squad.getHuman()) {
            String message = "Player can not join an enemy squad!";
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .header("message", "Player can not join an enemy squad")
                    .body(message);
        } else if (squadMemberRepository.existsByPlayerPlayerIdAndIsActiveIsTrue(player.getPlayerId())) {
            String message = "Player is already in a squad";
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }

        if (playerRepository.findAllBySquadMemberSquadSquadId(squadId).size() >= 1) {
            squadMember.setRank("Member");
        } else {
            squadMember.setRank("Leader");
        }
        squadMember.setKilled(false);
        squadMember.setActive(true);
        squadMember.setPlayer(player);
        SquadMember returnSquadMember = squadMemberRepository.save(squadMember);
        URI location = URI.create(String.format("/game/%d/squad/%d", gameId, squadId));
        return ResponseEntity.created(location)
                .header("message", "Squad Member added successful!")
                .body(returnSquadMember);
    }

    /**
     * This method is used to update a squadMember when they leave.
     *
     * @param gameId  is the id of the game the squadMember is in.
     * @param squadId is the id of the squad the squadMember is in.
     * @param player  is the player of the squadMember that wants to leave.
     * @return 200 if squadMember leaves the squad successfully. 400 if mismatch in information.
     * 404 if game, squadMember or squad could not be found.
     */
    @Operation(summary = "Update a squadMember to leave a squad.", security = @SecurityRequirement(name = "Bearer-token"))
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "SquadMember to be updated.",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Player.class))},
            required = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "PUT request successful. Expected changes: " +
                    "Active is set to inactive. Squad is set to null."),
            @ApiResponse(responseCode = "400", description = "Bad Request. " +
                    "There must be a valid player in the request body. " +
                    "Player does not belong in given game. " +
                    "Given squad does not belong in given game. " +
                    "Given player does not belong in given squad. " +
                    "Game already completed. "),
            @ApiResponse(responseCode = "404", description = "Game or player not found"),
    })
    @PutMapping("/leave")
    @RolesAllowed({"Admin", "User"})
    public ResponseEntity<?> removeSquadMember(@Parameter(description = "Id of the game the squad is in")
                                               @PathVariable(value = "gameId") Long gameId,
                                               @Parameter(description = "Id of the squad the squadMember is in")
                                               @PathVariable(value = "squadId") Long squadId,
                                               @RequestBody Player player) {
        if (player == null || player.getPlayerId() == null) {
            String message = "There must be a player with a valid id in the request body";
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        if (!playerRepository.existsById(player.getPlayerId())) {
            String message = String.format("Player id (%d) could not be found",
                    player.getPlayerId());
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        player = playerRepository.getOne(player.getPlayerId());
        if (player.squadMemberGetter() == null) {
            String message = "Could not find player's squad member. Are you sure you're in a squad?";
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        } else if (!squadMemberRepository.existsById(player.squadMemberGetter())) {
            String message = String.format("Squad Member id (%d) could not be found",
                    player.squadMemberGetter());
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        SquadMember squadMember = squadMemberRepository.getOne(player.squadMemberGetter());

        if (!squadRepository.existsById(squadId)) {
            String message = String.format("Squad id (%d) in path could not be found", squadId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        } else if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game id (%d) in path could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        } else if (!squadMember.getGame().getGameId().equals(gameId)) {
            String message = String.format("Game id (%d) in path does not match game id (%d) for squad member",
                    gameId, squadMember.getGame().getGameId());
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        } else if (!squadMember.getSquad().getSquadId().equals(squadId)) {
            String message = String.format("Squad id (%d) in path does not match squad id (%d) for squad member",
                    squadId, squadMember.getSquad().getSquadId());
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        } else if (!squadRepository.existsSquadBySquadIdAndGameGameId(squadId, gameId)) {
            String message = String.format("Squad id(%d) does not belong in game(%d)", squadId, gameId);
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        } else if (!playerRepository.existsByGameGameIdAndPlayerId(gameId, squadMember.getPlayer().getPlayerId())) {
            String message = String.format("Player id (%d) does not belong in game (%d)",
                    player.getGame().getGameId(), gameId);
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        Game game = gameRepository.getOne(gameId);
        if (game.getGameState().getStateId().equals(3L)) {
            String message = String.format("Game (%d) is already completed.", gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        squadMember.setActive(false);
        squadMember.setSquad(null);
        squadMemberRepository.save(squadMember);

        return new ResponseEntity<>("You have now left the squad.", HttpStatus.OK);
    }
}
