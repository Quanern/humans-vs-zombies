package com.noroff.no.server.services;

import com.noroff.no.server.models.User;
import com.noroff.no.server.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;
@Service
public class UserService {
    /*
    UserController, backend> create endpoint for creating a user

     */
    @Autowired
    private UserRepository userRepository;
    Logger logger = LoggerFactory.getLogger(UserService.class);
    public boolean IfUserExists(String email){
        return userRepository.existsByEmail(email);
    }

    public void createNewUserProfileFromJWT(Jwt principal){
        User user = new User();
        user.setUsername(principal.getClaimAsString("preferred_username"));
        user.setEmail(principal.getClaimAsString("email"));
        for (String role : principal.getClaimAsStringList("roles")){
            if (role.equals("Admin")){
                user.setAdmin(Boolean.TRUE);
            }
        }
        logger.info(String.format("Creating User(%s) with email(%s)", user.getUsername(), user.getEmail()));
        userRepository.save(user);
    }
}
