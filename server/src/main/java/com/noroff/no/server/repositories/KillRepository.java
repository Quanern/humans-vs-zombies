package com.noroff.no.server.repositories;

import com.noroff.no.server.models.Kill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KillRepository extends JpaRepository<Kill, Long> {
    List<Kill> findAllByGameGameId(Long gameId);
    Boolean existsByVictimPlayerId(Long victimId);
    Boolean existsByGameGameIdAndKillId(Long gameId, Long killId);
    Kill findByVictimPlayerId(Long victimId);
}
