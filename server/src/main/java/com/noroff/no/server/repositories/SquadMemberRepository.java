package com.noroff.no.server.repositories;

import com.noroff.no.server.models.SquadMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface SquadMemberRepository extends JpaRepository<SquadMember, Long> {
    Boolean existsByPlayerPlayerIdAndIsActiveIsTrue(Long playerId);
}
