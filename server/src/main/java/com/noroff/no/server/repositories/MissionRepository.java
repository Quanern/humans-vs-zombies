package com.noroff.no.server.repositories;

import com.noroff.no.server.models.Mission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MissionRepository extends JpaRepository<Mission, Long> {
    List<Mission> findAllByGameGameId(Long gameId);
    Boolean existsMissionByMissionIdAndGameGameId(Long missionId, Long gameId);
    Mission findMissionByMissionIdAndGameGameId(Long missionId, Long gameId);

    @Query(value = "SELECT MAX(mission_id)+1 AS new_id FROM missions", nativeQuery = true)
    Long generateNewMissionId();
}
