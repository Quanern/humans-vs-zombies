package com.noroff.no.server.repositories;

import com.noroff.no.server.models.SquadCheckin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SquadCheckinRepository extends JpaRepository<SquadCheckin, Long> {
    @Query(value = "select sqc.* from squad_checkins sqc inner join (select MAX(squad_checkin_id) AS squad_checkin_id, " +
            "squad_member_id from squad_checkins group by squad_member_id) sqcgrp " +
            "ON sqc.squad_checkin_id = sqcgrp.squad_checkin_id " +
            "AND sqc.squad_member_id = sqcgrp.squad_member_id WHERE squad_id = ?1",
            nativeQuery = true)
    List<SquadCheckin> getLatestCheckinBySquadId(Long squad_id);

    @Query(value = "select sqc.* from squad_checkins sqc inner join (select MAX(squad_checkin_id) AS squad_checkin_id, " +
            "squad_member_id from squad_checkins group by squad_member_id) sqcgrp " +
            "ON sqc.squad_checkin_id = sqcgrp.squad_checkin_id " +
            "AND sqc.squad_member_id = sqcgrp.squad_member_id WHERE game_id = ?1",
            nativeQuery = true)
    List<SquadCheckin> getLatestCheckinByGameId(Long gameId);
}
