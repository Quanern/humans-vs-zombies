package com.noroff.no.server.controller;

import com.noroff.no.server.models.Game;
import com.noroff.no.server.models.Kill;
import com.noroff.no.server.models.Mission;
import com.noroff.no.server.repositories.GameRepository;
import com.noroff.no.server.repositories.MissionRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.net.URI;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("api/v1/game")
@RestController
public class MissionController {
    private final MissionRepository missionRepository;
    private final GameRepository gameRepository;

    public MissionController(MissionRepository missionRepository, GameRepository gameRepository) {
        this.missionRepository = missionRepository;
        this.gameRepository = gameRepository;
    }

    /**
     * This method is used to get all missions in a game
     *
     * @param gameId id of the game to get all the missions from.
     * @return 200 if get request is successful and there are missions in the game.
     * 204 if there are no missions. 404 if the game id could not be found.
     */
    @Operation(summary = "Get all missions in a game.", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Mission.class))),
            @ApiResponse(responseCode = "204", description = "No content, no missions in the game"),
            @ApiResponse(responseCode = "404", description = "Game not found"),
    })
    @GetMapping("{gameId}/mission")
    public ResponseEntity<?> getAllMissionByGame(@Parameter(description = "Id of the game to get all mission for")
                                                 @PathVariable(value = "gameId") Long gameId) {

        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game with id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }
        // Gets all missions in specific game
        List<Mission> missionList = missionRepository.findAllByGameGameId(gameId);

        if (missionList.size() == 0) {
            // if there are no missions in the game, return no content
            String message = String.format("No mission found in game(%d)", gameId);
            return new ResponseEntity<>(message, HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok(missionList);
    }

    /**
     * This method is used to get a specific mission in a game
     *
     * @param gameId    is the id of the game the mission is in
     * @param missionId is the id of the mission to get
     * @return 200 and the mission if get request is successful.
     * 404 if mission or game could not be found. 400 if mismatch in information.
     */
    @Operation(summary = "Get a mission in a game.", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Mission.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request, mismatch in information"),
            @ApiResponse(responseCode = "404", description = "Game or mission not found"),
    })
    @GetMapping("{gameId}/mission/{missionId}")
    public ResponseEntity<?> getSpecificMissionByGame(@Parameter(description = "Id of the game the mission is in")
                                                      @PathVariable(value = "gameId") Long gameId,
                                                      @Parameter(description = "Id of the mission to get")
                                                      @PathVariable(value = "missionId") Long missionId) {
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game with id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        if (!missionRepository.existsById(missionId)) {
            String message = String.format("Mission with id (%d) could not be found", missionId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        if (!missionRepository.existsMissionByMissionIdAndGameGameId(missionId, gameId)) {
            // if specific mission is not found
            String message = String.format("Mission with id(%d) in game(%d) does not exist!", missionId, gameId);
            return ResponseEntity.badRequest().header("message", message).body(message);
        }

        Mission mission = missionRepository.findMissionByMissionIdAndGameGameId(missionId, gameId);
        return ResponseEntity.ok(mission);

    }

    /**
     * This method is used to create a new mission
     *
     * @param gameId  is the id of the game to create a new mission in
     * @param mission is the mission to create.
     * @return 201 and the created mission if created successfully. 400 if mismatch in information.
     * 404 if game could not be found.
     */
    @Operation(summary = "Create a new mission in a game", security = @SecurityRequirement(name = "Bearer-token"))
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Mission to be posted.",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Mission.class))},
            required = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "POST request successful"),
            @ApiResponse(responseCode = "400", description = "Bad Request. Game is completed. " +
                    "Mission is not visible for any faction."),
            @ApiResponse(responseCode = "404", description = "Game not found"),
    })
    @PostMapping("{gameId}/mission")
    @RolesAllowed("Admin")
    public ResponseEntity<?> createMission(@Parameter(description = "Id of the game to create a mission in")
                                           @PathVariable(value = "gameId") Long gameId,
                                           @RequestBody Mission mission) {
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        Game game = gameRepository.getOne(gameId);
        if (game.getGameState().getStateId().equals(3L)) {
            String message = String.format("Game (%d) is already completed.", gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }

        if (mission.getGame() == null) {
            // if the gameId is only supplied in path and not in body
            mission.setGame(new Game(gameId));
        }
        if (!mission.getHumanVisible() && !mission.getZombieVisible()) {
            String message = "The mission must be visible for at least one faction.";
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        Mission returnMission = missionRepository.save(mission);
        URI location = URI.create(String.format("/game/%d/mission/%d", gameId, mission.getMissionId()));
        return ResponseEntity.created(location)
                .header("message", "Mission added successful!")
                .body(returnMission);
    }

    /**
     * This method is used to update a mission
     *
     * @param gameId    is the id of the game the mission is in
     * @param missionId is the id of the mission to update
     * @param mission   is the updated mission
     * @return 200 if update was successful. 404 if game or mission could not be found. 400 if mismatch in information.
     */
    @Operation(summary = "Update a mission", security = @SecurityRequirement(name = "Bearer-token"))
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Updated mission.",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Mission.class))},
            required = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "PUT request successful. Expected changes:" +
                    "Mission title, mission description, mission time, mission location."),
            @ApiResponse(responseCode = "400", description = "Bad Request. Game is completed. "),
            @ApiResponse(responseCode = "404", description = "Game or mission not found"),
    })
    @PutMapping("{gameId}/mission/{missionId}")
    @RolesAllowed("Admin")
    public ResponseEntity<?> updateMission(@Parameter(description = "Id of the game the mission is in")
                                           @PathVariable(value = "gameId") Long gameId,
                                           @Parameter(description = "Id of the mission to update")
                                           @PathVariable(value = "missionId") Long missionId,
                                           @RequestBody Mission mission) {

        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game with id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }
        Game game = gameRepository.getOne(gameId);
        if (game.getGameState().getStateId().equals(3L)) {
            String message = String.format("Game (%d) is already completed.", gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        if (!missionRepository.existsById(missionId)) {
            String message = String.format("Mission with id (%d) could not be found", missionId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }
        if (!missionRepository.existsMissionByMissionIdAndGameGameId(missionId, gameId)) {
            // if mission doesn't exist
            String message = String.format("Mission with id(%d) and in game(%d) does not exist", missionId, gameId);
            return ResponseEntity.badRequest().header("message", message).body(message);
        } else if (mission.getMissionId() != null && !mission.getMissionId().equals(missionId) ||
                mission.getGame() != null && !mission.getGame().getGameId().equals(gameId)) {
            // if path variable and body has different missionIds or gameIds
            String message = "Values in path and body differ!";
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        } else {

            if (mission.getMissionId() == null) {
                // if the missionId is only supplied in path
                mission.setMissionId(missionId);
            }
            if (mission.getGame() == null) {
                // if the gameId is only supplied in path
                mission.setGame(new Game(gameId));
            }
            Mission oldMission = missionRepository.getOne(missionId);
            if (mission.getZombieVisible() != null) {
                oldMission.setZombieVisible(mission.getZombieVisible());
            }
            if (mission.getHumanVisible() != null) {
                oldMission.setHumanVisible(mission.getHumanVisible());
            }
            if (mission.getName() != null) {
                oldMission.setName(mission.getName());
            }
            if (mission.getGame() != null) {
                oldMission.setGame(mission.getGame());
            }
            if (mission.getDescription() != null) {
                oldMission.setDescription(mission.getDescription());
            }
            if (mission.getEndTime() != null) {
                oldMission.setEndTime(mission.getEndTime());
            }
            if (mission.getStartTime() != null) {
                oldMission.setStartTime(mission.getStartTime());
            }
            if (mission.getLat() != null) {
                oldMission.setLat(mission.getLat());
            }
            if (mission.getLng() != null) {
                oldMission.setLng(mission.getLng());
            }
            if (!oldMission.getHumanVisible() && !oldMission.getZombieVisible()) {
                String message = "The mission must be visible for at least one faction.";
                return ResponseEntity.badRequest()
                        .header("message", message)
                        .body(message);
            }

            Mission returnMission = missionRepository.save(oldMission);
            return ResponseEntity.ok()
                    .header("message", "edit successful!")
                    .body(returnMission);
        }
    }

    /**
     * This method is used to delete a mission
     *
     * @param gameId    is the id of the game the mission is in
     * @param missionId is the id of the mission to delete
     * @return 200 if deleted successfully. 404 if game or mission could not be found. 404 if mismatch in information.
     */
    @Operation(summary = "Delete a mission.", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "DELETE request successful. Expected changes: " +
                    "Removal of the mission from the database."),
            @ApiResponse(responseCode = "400", description = "Bad Request. Game already completed. " +
                    "Given mission id does not belong in given game id. "),
            @ApiResponse(responseCode = "404", description = "Mission or game not found"),
    })
    @DeleteMapping("{gameId}/mission/{missionId}")
    @RolesAllowed("Admin")
    public ResponseEntity<?> deleteMission(@Parameter(description = "Id of the game the mission is in")
                                           @PathVariable(value = "gameId") Long gameId,
                                           @Parameter(description = "Id of the mission to delete")
                                           @PathVariable(value = "missionId") Long missionId) {
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game with id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        Game game = gameRepository.getOne(gameId);
        if (game.getGameState().getStateId().equals(3L)) {
            String message = String.format("Game (%d) is already completed.", gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }

        if (!missionRepository.existsById(missionId)) {
            String message = String.format("Mission with id (%d) could not be found", missionId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        if (!missionRepository.existsMissionByMissionIdAndGameGameId(missionId, gameId)) {
            // if the mission is not found
            String message = String.format("Mission with id(%d) and in game(%d) does not exist", missionId, gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);

        }

        Mission mission = missionRepository.findMissionByMissionIdAndGameGameId(missionId, gameId);
        missionRepository.delete(mission);
        return ResponseEntity.ok()
                .header("message", "Delete successful!")
                .body(mission);
    }

}
