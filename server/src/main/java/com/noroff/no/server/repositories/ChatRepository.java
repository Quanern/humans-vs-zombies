package com.noroff.no.server.repositories;

import com.noroff.no.server.models.Chat;
import com.noroff.no.server.models.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatRepository extends JpaRepository<Chat, Long> {
    List<Chat> findAllByGameGameId(Long gameId);
    List<Chat> findAllByGameGameIdAndHumanGlobalIsTrue(Long gameId);
    List<Chat> findAllByGameGameIdAndZombieGlobalIsTrue(Long gameId);
    List<Chat> findAllBySquadSquadIdAndHumanGlobalIsFalseAndZombieGlobalIsFalse(Long squadId);
}
