package com.noroff.no.server.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "squads")
public class Squad {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "squad_id")
    private Long squadId;

    @Column(name = "name")
    private String name;

    @Column(name = "is_human")
    private Boolean isHuman;

    @ManyToOne
    @JoinColumn(name = "game_id", nullable = false)
    private Game game;

    @ManyToOne
    @JoinColumn(name = "player_id")
    private Player player;

    @OneToMany(mappedBy = "squad", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<SquadMember> squadMembers;

    @Column(name="deceased")
    private Long deceased;

    public Squad(Long squadId) {
        this.squadId = squadId;
    }

    public Squad() {}

    @JsonGetter("player")
    public Long playerGetter(){
        if(player == null){
            return null;
        }
        return player.getPlayerId();
    }
    @JsonGetter("squadMembers")
    public List<SquadMember> squadMembersGetter(){
        List<SquadMember> list = new ArrayList<>();
        if (squadMembers != null) {

            for (SquadMember squadMember: squadMembers) {
                    if(squadMember.getKilled() || squadMember.getActive()){
                        list.add(squadMember);
                    }
            }
            return list;
        }
        return null;
    }

    @JsonGetter("game")
    public Long gameGetter(){
        if(game == null){
            return null;
        }
        return game.getGameId();
    }


    @JsonSetter("game")
    public void gameSetter(Long gameId){
        this.setGame(new Game(gameId));
    }

    public Long getSquadId() {
        return squadId;
    }

    public void setSquadId(Long squadId) {
        this.squadId = squadId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getHuman() {
        return isHuman;
    }

    public void setHuman(Boolean human) {
        isHuman = human;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Long getDeceased() {
        return deceased;
    }

    public void setDeceased(Long deceased) {
        this.deceased = deceased;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public List<SquadMember> getSquadMembers() {
        return squadMembers;
    }

    public void setSquadMembers(List<SquadMember> squadMembers) {
        this.squadMembers = squadMembers;
    }
}
