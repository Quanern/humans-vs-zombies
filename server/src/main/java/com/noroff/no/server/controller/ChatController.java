package com.noroff.no.server.controller;

import com.noroff.no.server.models.*;
import com.noroff.no.server.repositories.*;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.security.RolesAllowed;
import java.net.URI;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
// TODO: Change the URL of chats
@RequestMapping("/api/v1/game/{gameId}/chat")
public class ChatController {
    private final ChatRepository chatRepository;
    private final PlayerRepository playerRepository;
    private final GameRepository gameRepository;
    private final SquadRepository squadRepository;
    private final SquadMemberRepository squadMemberRepository;

    public ChatController(ChatRepository chatRepository, PlayerRepository playerRepository, GameRepository gameRepository, SquadRepository squadRepository, SquadMemberRepository squadMemberRepository) {
        this.chatRepository = chatRepository;
        this.playerRepository = playerRepository;
        this.gameRepository = gameRepository;
        this.squadRepository = squadRepository;
        this.squadMemberRepository = squadMemberRepository;
    }

    /**
     * Method to get all the chats in a game. This includes global, faction and squad chat.
     *
     * @param gameId   is the id of the game the user wants to get the chats from
     * @param playerId is id of the player that wants the chats.
     * @return returns a list of the chats in the body and an OK response. Not Found, No Content and Bad Request
     * if there are any errors in the request body / path.
     */
    @Operation(summary = "Get all chats in a game by game Id and player Id.", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Chat.class))}),
            @ApiResponse(responseCode = "204", description = "There are no chats in this game for given playerId"),
            @ApiResponse(responseCode = "404", description = "The player with playerId given in path could not be found" +
                    "The game with gameId given in path could not be found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "The player does not belong to the game given in path" +
                    "information in player.", content = @Content)})
    @GetMapping("/{playerId}")
    @RolesAllowed({"Admin", "User"})
    public ResponseEntity<?> getAllChats(@Parameter(description = "Id of the game to get chats from")
                                         @PathVariable(value = "gameId") Long gameId,
                                         @Parameter(description = "Id of the player that gets the chats")
                                         @PathVariable(value = "playerId") Long playerId) {

        // Checks if the playerId in the path exists.
        if (!playerRepository.existsById(playerId)) {
            String message = String.format("No player found with id (%d)", playerId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }
        Player player = playerRepository.getOne(playerId);
        Boolean isHuman = player.getHuman();

        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game with id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }

        // Checks if the player is in the game
        if (!player.getGame().getGameId().equals(gameId)) {
            String message = String.format("Game id in path (%d) does not match player's game id (%d)",
                    gameId, player.getGame().getGameId());
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        // Player gets the human chat if they are human and zombie chat if not. Since global chat are marked as
        // both zombie and human chat true, they will always receive them.
        List<Chat> gameChats;
        if (isHuman) {
            gameChats = chatRepository.findAllByGameGameIdAndHumanGlobalIsTrue(gameId);
        } else {
            gameChats = chatRepository.findAllByGameGameIdAndZombieGlobalIsTrue(gameId);
        }
        if (gameChats.size() == 0) {
            String message = String.format("No chats found in game(%d)", gameId);
            return new ResponseEntity<>(message, HttpStatus.NO_CONTENT);

        }
        // Checks if the player is in a squad or not. If they are then they will get the faction chat as well.
        Long squadId = null;
        if (player.squadMemberGetter() != null) {
            SquadMember squadMember = squadMemberRepository.getOne(player.squadMemberGetter());
            squadId = squadMember.getSquad().getSquadId();
        }
        if (squadId != null) {
            List<Chat> squadChats = chatRepository.findAllBySquadSquadIdAndHumanGlobalIsFalseAndZombieGlobalIsFalse(squadId);
            if (squadChats.size() > 0) {
                List<Chat> allChats = new ArrayList<>();
                allChats.addAll(gameChats);
                allChats.addAll(squadChats);
                // This sorts the elements on the combined array by the timestamp.
                allChats.sort(Comparator.comparing(Chat::getTimestamp));
                gameChats = allChats;
            }
        }
        return ResponseEntity.ok()
                .header("message", "chats fetched successful!")
                .body(gameChats);
    }

    /**
     * Method for admin to get all chats in a game except squad chat.
     *
     * @param gameId is the id of the game the admin wants the chats from.
     * @return 200 a list of all the chats. 204, no content. 404 game not found.
     */
    @Operation(summary = "Get all chats in a game by game Id for admin.", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Chat.class))}),
            @ApiResponse(responseCode = "204", description = "There are no chats in this game"),
            @ApiResponse(responseCode = "404", description = "Game not found")})
    @GetMapping()
    @RolesAllowed({"Admin"})
    public ResponseEntity<?> getAllChatsAdmin(@Parameter(description = "Id of the game to get chats from")
                                              @PathVariable(value = "gameId") Long gameId) {

        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game with id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }

        // Player gets the human chat if they are human and zombie chat if not. Since global chat are marked as
        // both zombie and human chat true, they will always receive them.
        List<Chat> gameChats = chatRepository.findAllByGameGameId(gameId);
        if (gameChats.size() == 0) {
            String message = String.format("No chats found in game(%d)", gameId);
            return new ResponseEntity<>(message, HttpStatus.NO_CONTENT);

        }
        gameChats.sort(Comparator.comparing(Chat::getTimestamp));
        return ResponseEntity.ok()
                .header("message", "chats fetched successful!")
                .body(gameChats);
    }

    /**
     * This method is used to create a new faction or global chat. If you want to add a squad chat, see
     * SquadController.java.
     *
     * @param gameId    is the id of the game the user wants to post a chat in.
     * @param chat      is the Chat object that the user wants to be posted.
     * @param principal is the Jwt needed for authentication. Authorized admins are not required to be a player to post.
     * @return 404 Not Found if player in Chat or game could not be found
     * 400 Bad Request if there are mismatching game Ids in chat and the path.
     * 201 Created if the Chat got posted. Returns the chat object.
     */
    @Operation(summary = "Post a new chat in a game with it's Id and a Chat object with player identity.",
            security = @SecurityRequirement(name = "Bearer-token"))
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Chat to be posted.",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Chat.class))},
            required = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "POST request successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Chat.class))}),
            @ApiResponse(responseCode = "404", description = "Game or Player not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Bad request. " +
                    "Game id in path does not match game id of player. " +
                    "Sending messages to wrong/no faction. " +
                    "Sending an empty message. " +
                    "Sending a message without a player id attached without being an admin")})
    @PostMapping
    @RolesAllowed({"Admin", "User"})
    public ResponseEntity<?> addNewChat(@Parameter(description = "Id of the game to chat in")
                                        @PathVariable(value = "gameId") Long gameId,
                                        @RequestBody Chat chat,
                                        @AuthenticationPrincipal Jwt principal) {
        ResponseEntity<?> result = null;
        if (chat.getMessage().equals("") || chat.getMessage() == null) {
            String message = "Chat cannot have an empty message";
            result = ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        } else {
            if (!principal.getClaimAsStringList("roles").contains("Admin")) {
                if (chat.getPlayer() == null) {
                    String message = "You must include a player with their id in the body";
                    result = ResponseEntity.badRequest()
                            .header("message", "You must include a player with their id in the body")
                            .body(message);
                } else if (!playerRepository.existsById(chat.getPlayer().getPlayerId())) {
                    String message = String.format("Player id (%d) could not be found", chat.getPlayer().getPlayerId());
                    result = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
                } else {
                    Player player = playerRepository.getOne(chat.getPlayer().getPlayerId());
                    Boolean human = player.getHuman();// If there is not set any globals, humans will automatically post to humans and zombies to zombies.
                    if (chat.getZombieGlobal() == null) {
                        chat.setZombieGlobal(!human);
                    }
                    if (chat.getHumanGlobal() == null) {
                        chat.setHumanGlobal(human);
                    }// Checks if the player's game Id is the same as the path's game Id.
                    if (!player.getGame().getGameId().equals(gameId)) {
                        String message = String.format("Game in path (%d) does not match the game the player is in (%d)",
                                gameId, player.getGame().getGameId());
                        result = ResponseEntity.badRequest()
                                .header("message", message)
                                .body(message);
                    }
                    // Check if the player sends a message to the wrong faction if it isn't global
                    else if (human && !chat.getHumanGlobal() && chat.getZombieGlobal() || !human && chat.getHumanGlobal() && !chat.getZombieGlobal()) {
                        String message = "You can not send messages to only the enemy faction";
                        result = ResponseEntity.badRequest()
                                .header("message", message)
                                .body(message);
                    }
                }

            }
            if (result == null) {// If there is no game defined in the requestBody, the chat will set the gameId to the path's gameId
                if (chat.getGame() == null || chat.getGame().getGameId() == null) {
                    chat.setGame(new Game(gameId));
                } else if (!chat.getGame().getGameId().equals(gameId)) {
                    String message = String.format("Game id in path (%d) does not match gameId in body(%d)",
                            gameId, chat.getGame().getGameId());
                    result = ResponseEntity.badRequest()
                            .header("message", message)
                            .body(message);
                }
                if (result == null) {// Checks that the message is sent to at least one faction.
                    if (!chat.getHumanGlobal() && !chat.getZombieGlobal()) {
                        String message = "You must send a message to faction or global";
                        result = ResponseEntity.badRequest()
                                .header("message", message)
                                .body(message);
                    } else {// Sets the squad to null since this isn't the api for squad chat. Sets the timestamp and posts the chat.
                        chat.setSquad(null);
                        chat.setTimestamp(new Date());
                        Chat returnChat = chatRepository.save(chat);
                        URI location = URI.create(String.format("/game/%d/chat", gameId));
                        result = ResponseEntity.created(location)
                                .header("message", "Chat added successful!")
                                .body(returnChat);
                    }
                }
            }
        }

        return result;
    }
}
