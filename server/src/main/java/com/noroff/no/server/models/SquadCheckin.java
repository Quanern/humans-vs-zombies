package com.noroff.no.server.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "squad_checkins")
public class SquadCheckin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "squad_checkin_id")
    private Long squadCheckinId;

    @Column(name = "time")
    private Date time;

    @Column(name = "lat")
    private String lat;

    @Column(name = "lng")
    private String lng;

    @ManyToOne
    @JoinColumn(name = "game_id")
    private Game game;

    @ManyToOne
    @JoinColumn(name = "squad_id")
    private Squad squad;

    @ManyToOne
    @JoinColumn(name = "squad_member_id")
    private SquadMember squadMember;

    @JsonSetter("squadMember")
    public void squadMemberSetter(Long playerId){this.squadMember = new SquadMember(new Player(playerId));}

    public SquadCheckin() {}

    public SquadCheckin(Date time, Game game, Squad squad, SquadMember squadMember) {
        this.time = time;
        this.game = game;
        this.squad = squad;
        this.squadMember = squadMember;
    }

    @JsonGetter("game")
    public Long gameGetter(){
        if(game == null){
            return null;
        }
        return game.getGameId();
    }

    @JsonGetter("squad")
    public Long squadGetter(){
        if(squad == null){
            return null;
        }
        return squad.getSquadId();
    }
    @JsonGetter("squadMember")
    public String squadMemberGetter(){
        if(squadMember == null){
            return null;
        }
        return squadMember.getPlayer().getUser().getUsername();
    }

    @JsonSetter("game")
    public void gameSetter(Long gameId){
        this.setGame(new Game(gameId));
    }

    @JsonSetter("squad")
    public void squadSetter(Long squadId){
        this.setSquad(new Squad(squadId));
    }

    public Long getSquadCheckinId() {
        return squadCheckinId;
    }

    public void setSquadCheckinId(Long squadCheckinId) {
        this.squadCheckinId = squadCheckinId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game gameId) {
        this.game = gameId;
    }

    public Squad getSquad() {
        return squad;
    }

    public void setSquad(Squad squadId) {
        this.squad = squadId;
    }

    public SquadMember getSquadMember() {
        return squadMember;
    }

    public void setSquadMember(SquadMember squadMemberId) {
        this.squadMember = squadMemberId;
    }
}


