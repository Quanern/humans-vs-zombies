package com.noroff.no.server.controller;

import com.noroff.no.server.repositories.GameStateRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class GameStateController {
    private final GameStateRepository gameStateRepository;

    public GameStateController(GameStateRepository gameStateRepository) {
        this.gameStateRepository = gameStateRepository;
    }
}
