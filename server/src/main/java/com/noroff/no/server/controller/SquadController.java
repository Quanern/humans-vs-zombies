package com.noroff.no.server.controller;

import com.noroff.no.server.models.*;
import com.noroff.no.server.repositories.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.net.URI;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("api/v1/game")
@RestController
public class SquadController {
    private final SquadRepository squadRepository;
    private final ChatRepository chatRepository;
    private final PlayerRepository playerRepository;
    private final GameRepository gameRepository;
    private final SquadMemberRepository squadMemberRepository;
    private final SquadCheckinRepository squadCheckinRepository;

    public SquadController(SquadRepository squadRepository, ChatRepository chatRepository, PlayerRepository playerRepository, GameRepository gameRepository, SquadMemberRepository squadMemberRepository, SquadCheckinRepository squadCheckinRepository) {
        this.squadRepository = squadRepository;
        this.chatRepository = chatRepository;
        this.playerRepository = playerRepository;
        this.gameRepository = gameRepository;
        this.squadMemberRepository = squadMemberRepository;
        this.squadCheckinRepository = squadCheckinRepository;
    }

    /**
     * This method is used to get all squads in a game
     *
     * @param gameId is the id of the game to get squads from.
     * @return 200 if squads are fetched successfully. 204 if there are no content. 404 if game could not be found.
     */
    @Operation(summary = "Get all squads in a game.", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Squad.class))),
            @ApiResponse(responseCode = "404", description = "Game not found"),
    })
    @GetMapping("/{gameId}/squad")
    @RolesAllowed({"Admin", "User"})
    public ResponseEntity<?> getAllSquadsByGame(@Parameter(description = "Id of the game to get squads from")
                                                @PathVariable(value = "gameId") Long gameId) {
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("The game id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        // Gets all squads in a specific game
        List<Squad> squadList = squadRepository.findAllByGameGameId(gameId);

        if (squadList.size() == 0) {
            // if there are no squads in the game, return no content
            String message = String.format("No squads found in game(%d)", gameId);
            return new ResponseEntity<>(message, HttpStatus.NO_CONTENT);

        }
        return ResponseEntity.ok(squadList);
    }

    /**
     * This method is used to get a specific squad in a game.
     *
     * @param gameId  is the id of the game to look for the squad in.
     * @param squadId is the id of the squad to get.
     * @return 200 if squad is fetched successfully. 400 if mismatch in information.
     * 404 if squad or game could not be found.
     */
    @Operation(summary = "Get a specific squad in a game.", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Squad.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request. " +
                    "Given squad does not belong in given game. "),
            @ApiResponse(responseCode = "404", description = "Game or squad not found"),
    })
    @GetMapping("{gameId}/squad/{squadId}")
    @RolesAllowed({"Admin", "User"})
    public ResponseEntity<?> getSpecificSquadByGame(@Parameter(description = "Id of the game the squad is in")
                                                    @PathVariable(value = "gameId") Long gameId,
                                                    @Parameter(description = "Id of the squad")
                                                    @PathVariable(value = "squadId") Long squadId) {
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("The game id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        if (!squadRepository.existsById(squadId)) {
            String message = String.format("The squad id (%d) could not be found", squadId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        if (!squadRepository.existsSquadBySquadIdAndGameGameId(squadId, gameId)) {
            // If specific squad is not found
            String message = String.format("Squad with id(%d) and in game(%d) does not exist", squadId, gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);

        }
        //Get specific squad in a specific game.
        Squad squad = squadRepository.findSquadBySquadIdAndGameGameId(squadId, gameId);
        return ResponseEntity.ok(squad);
    }

    /**
     * This method is used to create a squad in a game
     *
     * @param gameId is the id of the game to create a squad in
     * @param squad  is the squad to create.
     * @return 201 if squad is successfully created. 404 if game is not found. 400 if mismatch in information.
     */
    @Operation(summary = "Create a squad", security = @SecurityRequirement(name = "Bearer-token"))
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Squad to be created.",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Squad.class))},
            required = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "POST request successful"),
            @ApiResponse(responseCode = "400", description = "Bad Request. " +
                    "Game id in path does not match game id in body. " +
                    "Game already completed. " +
                    "Player can not create squad for opposite faction. " +
                    "Player does not belong in given game id. " +
                    "Player is already in a squad. "),
            @ApiResponse(responseCode = "404", description = "Game or player not found"),
    })
    @PostMapping("{gameId}/squad")
    @RolesAllowed({"Admin", "User"})
    public ResponseEntity<?> createSquad(@Parameter(description = "Id of the game to create a squad in")
                                         @PathVariable(value = "gameId") Long gameId,
                                         @RequestBody Squad squad) {
        if (squad.getSquadId() == null) {
            // if there is no squadId given, one will be generated
            squad.setSquadId(squadRepository.generateNewSquadId());
        }

        if (squad.getGame() == null || squad.getGame().getGameId() == null) {
            // if the gameId is only supplied in path and not in body
            squad.setGame(new Game(gameId));
        }
        if (!squad.gameGetter().equals(gameId)) {
            String message = String.format("Game id (%d) of path does not match game id (%d) of squad",
                    gameId, squad.getGame().getGameId());
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game with id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }
        Game game = gameRepository.getOne(gameId);
        if (game.getGameState().getStateId().equals(3L)) {
            String message = String.format("Game (%d) is already completed.", gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        squad.setDeceased(0L);
        Squad returnSquad;
        // Checks if it is a player creating the squad
        if (squad.getPlayer() != null) {
            if (!playerRepository.existsById(squad.getPlayer().getPlayerId())) {
                String message = String.format("Player with id (%d) could not be found",
                        squad.getPlayer().getPlayerId());
                return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

            }
            Player player = playerRepository.getOne(squad.getPlayer().getPlayerId());
            if (squad.getHuman() == null) {
                squad.setHuman(player.getHuman());
            }
            // Checks if the faction of the squad is the same as the faction of the creator.
            if (player.getHuman() != squad.getHuman()) {
                String message = "You can not create a squad for the other faction";
                return ResponseEntity.badRequest()
                        .header("message", message)
                        .body(message);
            }
            // Checks if the game in the path (which might become the game in the body) matches the game for the player.
            else if (!player.getGame().getGameId().equals(gameId)) {
                String message = String.format("Game id (%d) of path does not match game id (%d) of player",
                        gameId, player.getGame().getGameId());
                return ResponseEntity.badRequest()
                        .header("message", message)
                        .body(message);
            }
            // Checks if the player is already in a squad or not.
            else if (player.squadMemberGetter() != null) {
                String message = "You can not create a squad while in a squad";
                return ResponseEntity.badRequest()
                        .header("message", message)
                        .body(message);
            }
            // Saves the squad
            returnSquad = squadRepository.save(squad);
            // Creates a squad member of the creator of the squad
            SquadMember squadMember = new SquadMember("Leader", returnSquad.getGame(), returnSquad, returnSquad.getPlayer(), true, false);
            squadMemberRepository.save(squadMember);
        } else {
            returnSquad = squadRepository.save(squad);
        }
        URI location = URI.create(String.format("/game/%d/squad/%d", gameId, squad.getSquadId()));
        return ResponseEntity.created(location)
                .header("message", "Squad added successful!")
                .body(returnSquad);
    }

    /**
     * this method is used to update a squad.
     *
     * @param gameId  is the id of the game where the squad is.
     * @param squadId is the id of the squad.
     * @param squad   is squad object to be updated to.
     * @return 200 if update is successful. 404 if squad or game could not be found. 400 if mismatch in information.
     */
    @Operation(summary = "Update a squad", security = @SecurityRequirement(name = "Bearer-token"))
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Squad to be updated.",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Squad.class))},
            required = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "PUT request successful. Expected changes: " +
                    "Name of the squad, number of deceased members. "),
            @ApiResponse(responseCode = "400", description = "Bad Request. " +
                    "Given squad does not belong in given game. " +
                    "Given squad or game does not match squad or game in body. " +
                    "Game already completed. "),
            @ApiResponse(responseCode = "404", description = "Game or squad not found"),
    })
    @PutMapping("{gameId}/squad/{squadId}")
    @RolesAllowed("Admin")
    public ResponseEntity<?> updateSquad(@Parameter(description = "Id of the game where the squad is")
                                         @PathVariable(value = "gameId") Long gameId,
                                         @Parameter(description = "Id of the squad to update")
                                         @PathVariable(value = "squadId") Long squadId,
                                         @RequestBody Squad squad) {

        if (!squadRepository.existsSquadBySquadIdAndGameGameId(squadId, gameId)) {
            // if squad doesn't exits
            String message = String.format("Squad with id(%d) and in game(%d) does not exist", squadId, gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        } else if (squad.getSquadId() != null && !squad.getSquadId().equals(squadId) ||
                squad.getGame() != null && !squad.getGame().getGameId().equals(gameId)) {
            // if path variable and body has different squadIds or gameIds
            String message = "Values in path and body differs";
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        } else if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        } else {
            Game game = gameRepository.getOne(gameId);
            if (game.getGameState().getStateId().equals(3L)) {
                String message = String.format("Game (%d) is already completed.", gameId);
                return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
            }
            Squad oldSquad = squadRepository.getOne(squadId);
            if (squad.getName() != null) {
                oldSquad.setName(squad.getName());
            }
            if (squad.getHuman() != null) {
                oldSquad.setHuman(squad.getHuman());
            }
            if (squad.getPlayer() != null) {
                oldSquad.setPlayer(squad.getPlayer());
            }
            if (squad.getDeceased() != null) {
                oldSquad.setDeceased(squad.getDeceased());
            }
            Squad returnSquad = squadRepository.save(oldSquad);
            return ResponseEntity.ok()
                    .header("message", "edit successful!")
                    .body(returnSquad);
        }
    }

    /**
     * This method is used to delete a squad.
     *
     * @param gameId  is the id of the game where the squad is.
     * @param squadId is the id of the squad.
     * @return 200 if delete is successful. 404 if game or squad could not be found. 400 if mismatch in information.
     */
    @Operation(summary = "Delete a squad.", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "DELETE request successful. Expected changes: " +
                    "Removal of the squad in the database."),
            @ApiResponse(responseCode = "400", description = "Bad Request. " +
                    "Game already completed. " +
                    "Given squad does not belong in given game. "),
            @ApiResponse(responseCode = "404", description = "Game or squad not found"),
    })
    @DeleteMapping("{gameId}/squad/{squadId}")
    @RolesAllowed("Admin")
    public ResponseEntity<?> deleteSquad(@Parameter(description = "Id of the game where the squad to delete is")
                                         @PathVariable(value = "gameId") Long gameId,
                                         @Parameter(description = "Id of the squad to delete")
                                         @PathVariable(value = "squadId") Long squadId) {

        if (!squadRepository.existsSquadBySquadIdAndGameGameId(squadId, gameId)) {
            // if the squad is not found
            String message = String.format("Squad with id(%d) and in game(%d) does not exist", squadId, gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game with id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }
        Game game = gameRepository.getOne(gameId);
        if (game.getGameState().getStateId().equals(3L)) {
            String message = String.format("Game (%d) is already completed.", gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        Squad squad = squadRepository.findSquadBySquadIdAndGameGameId(squadId, gameId);
        squadRepository.delete(squad);
        return new ResponseEntity<>("Delete successful!", HttpStatus.OK);
    }

    /**
     * This method is used to get squad chat.
     *
     * @param gameId   is the id of the game where the squad is.
     * @param squadId  is the id of the squad to get chat from.
     * @param playerId is the id of the player requesting the chat.
     * @return 200 if the get request is successful with a list of squad chats. 204 if no content.
     * 400 if mismatch in information. 404 if squad, game
     */
    @Operation(summary = "Get a squadChat", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful"),
            @ApiResponse(responseCode = "204", description = "There has been no squad messages in this squad."),
            @ApiResponse(responseCode = "400", description = "Bad Request. " +
                    "Given player does not belong in given squad. " +
                    "Given player does not belong in given game. " +
                    "Given squad does not belong in given game. " +
                    "Given player is not in a squad. "),
            @ApiResponse(responseCode = "404", description = "Game, player, squad or squadMember not found"),
    })
    @GetMapping("{gameId}/squad/{squadId}/chat/{playerId}")
    @RolesAllowed({"Admin", "User"})
    public ResponseEntity<?> getSquadChat(@Parameter(description = "Id of the game the squad is in")
                                          @PathVariable(value = "gameId") Long gameId,
                                          @Parameter(description = "Id of the squad to get chat from")
                                          @PathVariable(value = "squadId") Long squadId,
                                          @Parameter(description = "Id of the player that wants chat")
                                          @PathVariable(value = "playerId") Long playerId) {
        if (!playerRepository.existsById(playerId)) {
            String message = String.format("The player id (%d) could not be found.", playerId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        Player player = playerRepository.getOne(playerId);
        if (!squadRepository.existsById(squadId)) {
            String message = String.format("The squad id (%d) could not be found.", squadId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("The game id (%d) could not be found.", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        if (!squadRepository.existsSquadBySquadIdAndGameGameId(squadId, gameId)) {
            // if the squad is not found
            String message = String.format("Squad with id(%d) and in game(%d) does not exist", squadId, gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        if (player.squadMemberGetter() == null) {
            String message = "Player is not a member of a squad";
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        if (!squadMemberRepository.existsById(player.squadMemberGetter())) {
            String message = String.format("The squadMember id (%d) could not be found.", player.squadMemberGetter());
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        if (!player.getGame().getGameId().equals(gameId)) {
            String message = String.format("The game id (%d) for player is not the same as game id (%d) in path.",
                    player.getGame().getGameId(), gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        SquadMember squadMember = squadMemberRepository.getOne(player.squadMemberGetter());
        if (!squadMember.getSquad().getSquadId().equals(squadId)) {
            String message = String.format("The squad id (%d) for squadMember is not the same as squad id (%d) in path.",
                    squadMember.getSquad().getSquadId(), squadId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        List<Chat> squadChats = chatRepository.findAllBySquadSquadIdAndHumanGlobalIsFalseAndZombieGlobalIsFalse(squadId);
        if (squadChats.size() == 0) {
            String message = "No messages sent in squad chat";
            return new ResponseEntity<>(message, HttpStatus.NO_CONTENT);

        }
        return ResponseEntity.ok()
                .header("message", "chats fetched successful!")
                .body(squadChats);
    }

    /**
     * This method is used to create a new squad chat.
     *
     * @param gameId  is the id of the game where the squad is.
     * @param squadId is the id of the squad to send a message to.
     * @param chat    is the object to be created.
     * @param principal is the Jwt needed for authentication. Authorized admins are not required to be a player to post.
     * @return 201 if chat is successfully created. 404 if game, squad, squadMember or player could not be found.
     * 400 if mismatch in information.
     */
    @Operation(summary = "Create a squadChat", security = @SecurityRequirement(name = "Bearer-token"))
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "SquadChat to be created.",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Squad.class))},
            required = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "POST request successful"),
            @ApiResponse(responseCode = "400", description = "Bad Request. " +
                    "given squad does not belong in given game. " +
                    "No player in the request body. " +
                    "Player is not in a squad. " +
                    "Squad id in the path is not the squad the player is in. " +
                    "Player is not in given game. "),
            @ApiResponse(responseCode = "404", description = "Game, player, squad or squadMember not found"),
    })
    @PostMapping("{gameId}/squad/{squadId}/chat")
    @RolesAllowed({"Admin", "User"})
    public ResponseEntity<?> createSquadChat(@Parameter(description = "Id of the game where the squad is in")
                                             @PathVariable(value = "gameId") Long gameId,
                                             @Parameter(description = "Id of the squad to post a chat in")
                                             @PathVariable(value = "squadId") Long squadId,
                                             @RequestBody Chat chat,
                                             @AuthenticationPrincipal Jwt principal) {
        if (chat.getMessage().equals("") || chat.getMessage() == null) {
            String message = "Chat cannot have an empty message";
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);

        }
        if (chat.getGame() == null) {
            chat.setGame(new Game(gameId));
        }
        if (chat.getSquad() == null) {
            chat.setSquad(new Squad(squadId));
        }
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game with id(%d) does not exist", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        } else if (!squadRepository.existsById(squadId)) {
            String message = String.format("Squad with id(%d) does not exist", squadId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        } else if (!squadRepository.existsSquadBySquadIdAndGameGameId(squadId, gameId)) {
            String message = String.format("Squad with id(%d) and in game(%d) does not exist", squadId, gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }

        if (!principal.getClaimAsStringList("roles").contains("Admin")) {
            if (chat.getPlayer() == null) {
                String message = "There must be a player in the request";
                return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
            }
            if (!playerRepository.existsById(chat.getPlayer().getPlayerId())) {
                String message = String.format("Player id (%d) could not be found", chat.getPlayer().getPlayerId());
                return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
            }

            Player player = playerRepository.getOne(chat.getPlayer().getPlayerId());
            if (!chat.getGame().getGameId().equals(gameId)) {
                String message = "This player is not in the same game as the path's gameId";
                return ResponseEntity.badRequest()
                        .header("message", message)
                        .body(message);
            }
            if (player.squadMemberGetter() == null) {
                String message = "Player is not an active squad member";
                return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

            }
            SquadMember squadMember = squadMemberRepository.getOne(player.squadMemberGetter());
            if (!squadMember.getSquad().getSquadId().equals(squadId)) {
                String message = "Squad id in path is not the squad id of the player";
                return ResponseEntity.badRequest()
                        .header("message", message)
                        .body(message);
            }
        }

        chat.setTimestamp(new Date());
        chat.setHumanGlobal(false);
        chat.setZombieGlobal(false);
        Chat returnChat = chatRepository.save(chat);

        URI location = URI.create(String.format("/game/%d/squad/%d/chat", gameId, squadId));
        return ResponseEntity.created(location)
                .header("message", "Chat created successful!")
                .body(returnChat);
    }
}
