package com.noroff.no.server.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "players")
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "player_id")
    private Long playerId;

    @Column(name = "is_human")
    private Boolean isHuman;

    @Column(name = "bite_code")
    private String biteCode;

    @ManyToOne
    @JoinColumn(name = "game_id", nullable = false)
    private Game game;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @OneToMany(mappedBy = "killer", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Kill> kills;

    @OneToMany(mappedBy = "player", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Chat> chats;

    @OneToMany(mappedBy = "player", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<SquadMember> squadMember;

    @OneToMany(mappedBy = "player", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Squad> squads;

    public Player() {
    }

    public Player(String biteCode) {
        this.biteCode = biteCode;
    }

    public Player(Long playerId) {
        this.playerId = playerId;
    }

    @JsonGetter("game")
    public Long gameGetter() {
        return game.getGameId();
    }

    @JsonGetter("kills")
    public List<Long> killsGetter() {
        if (kills != null) {
            return kills.stream().map(kill -> {
                return kill.getKillId();
            }).collect(Collectors.toList());
        }
        return null;
    }

    @JsonGetter("squads")
    public List<Long> squadsGetter() {
        if (squads != null) {
            return squads.stream().map(squad -> {
                return squad.getSquadId();
            }).collect(Collectors.toList());
        }
        return null;
    }

    @JsonGetter("currentSquad")
    public Long currentSquadGetter() {
        if(squadMember != null){
            for (SquadMember squadMember: squadMember) {
                if(squadMember.getActive()){
                    return squadMember.getSquad().getSquadId();
                }
            }
        }
        return null;
    }

    @JsonGetter("chats")
    public List<String> chatsGetter() {
        if (chats != null) {
            return chats.stream().map(chat -> {
                return chat.getTimestamp() + ": " + chat.getMessage();
            }).collect(Collectors.toList());
        }
        return null;
    }

    @JsonGetter("squadMember")
    public Long squadMemberGetter() {
        if(squadMember != null){
            for (SquadMember squadMember: squadMember) {
                if (squadMember.getActive()){
                    return squadMember.getSquadMemberId();
                }
            }
        }
        return null;
    }

    @JsonSetter("game")
    public void gameSetter(Long gameId){
        this.setGame(new Game(gameId));
    }

    @JsonSetter("user")
    public void userSetter(String username){
        this.setUser(new User(username));
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public Boolean getHuman() {
        return isHuman;
    }

    public void setHuman(Boolean human) {
        isHuman = human;
    }

    public String getBiteCode() {
        return biteCode;
    }

    public void setBiteCode(String biteCode) {
        this.biteCode = biteCode;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Kill> getKills() {
        return kills;
    }

    public void setKills(Set<Kill> kills) {
        this.kills = kills;
    }

    public Set<Chat> getChats() {
        return chats;
    }

    public void setChats(Set<Chat> chats) {
        this.chats = chats;
    }

    public List<SquadMember> getSquadMember() {
        return squadMember;
    }

    public void setSquadMember(List<SquadMember> squadMembers) {
        this.squadMember = squadMembers;
    }

    public List<Squad> getSquads() {
        return squads;
    }

    public void setSquads(List<Squad> squads) {
        this.squads = squads;
    }
}
