package com.noroff.no.server.controller;

import com.noroff.no.server.models.User;
import com.noroff.no.server.repositories.UserRepository;
import com.noroff.no.server.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("api/v1/user")
@RestController
public class UserController {
    private final UserRepository userRepository;
    private final UserService userService;

    public UserController(UserRepository userRepository, UserService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }

    Logger logger = LoggerFactory.getLogger(UserController.class);

    @Operation(description = "Log in method")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Login successful")
    })
    @PostMapping()
    public ResponseEntity<?> logIn(@AuthenticationPrincipal Jwt principal) {
        String username = principal.getClaimAsString("preferred_username");
        logger.info(String.format("%s logged in.", username));

        String email = principal.getClaimAsString("email");
        if (!userService.IfUserExists(email)) {
            // registers user if user is not already in the db
            userService.createNewUserProfileFromJWT(principal);
        }
        return ResponseEntity.ok()
                .body("login successful!");
    }

    @Operation(description = "Getting userinfo")
    @GetMapping()
    public ResponseEntity<?> getOne(@AuthenticationPrincipal Jwt principal) {
        User user = userRepository.getOne(principal.getClaimAsString("email"));
        return ResponseEntity.ok().body(user);
    }
}
