package com.noroff.no.server.repositories;

import com.noroff.no.server.models.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {
    Boolean existsByGameGameIdAndPlayerId(Long gameId, Long playerId);
    Boolean existsByGameGameIdAndUserUserId(Long gameId, Long userId);
    Boolean existsByBiteCodeAndGameGameId(String biteCode, Long gameId);
    Player findByBiteCodeAndGameGameId(String biteCode, Long gameId);
    List<Player> findAllBySquadMemberSquadSquadId(Long squadId);
    List<Player> findAllByGameGameId(Long gameId);
}
