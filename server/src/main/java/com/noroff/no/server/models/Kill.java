package com.noroff.no.server.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table (name = "kills")
public class Kill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "kill_id")
    private Long killId;

    @Column(name = "time_of_death")
    private Date timeOfDeath;

    @Column(name = "story")
    private String story;

    @Column(name = "lat")
    private String lat;

    @Column(name = "lng")
    private String lng;

    @ManyToOne
    @JoinColumn(name = "game_id", nullable = false)
    private Game game;

    @ManyToOne
    @JoinColumn(name = "killer_id", nullable = false)
    private Player killer;

    @OneToOne
    @JoinColumn(name = "victim_id", nullable = false)
    private Player victim;

    @JsonGetter("game")
    public Long gameGetter() {
        return game.getGameId();
    }

    @JsonGetter("killer")
    public Long killerGetter() {
        return killer.getPlayerId();
    }

    @JsonGetter("victim")
    public Long victimGetter() {
        return victim.getPlayerId();
    }

    @JsonSetter("game")
    public void gameSetter(Long gameId){
        this.setGame(new Game(gameId));
    }

    @JsonSetter("victim")
    public void victimSetter(String biteCode){
        this.setVictim(new Player(biteCode));
    }

    @JsonSetter("killer")
    public void killerSetter(Long playerId){
        this.setKiller(new Player(playerId));
    }

    public Long getKillId() {
        return killId;
    }

    public void setKillId(Long killId) {
        this.killId = killId;
    }

    public Date getTimeOfDeath() {
        return timeOfDeath;
    }

    public void setTimeOfDeath(Date timeOfDeath) {
        this.timeOfDeath = timeOfDeath;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Player getKiller() {
        return killer;
    }

    public void setKiller(Player killer) {
        this.killer = killer;
    }

    public Player getVictim() {
        return victim;
    }

    public void setVictim(Player victim) {
        this.victim = victim;
    }
}
