package com.noroff.no.server.controller;

import com.noroff.no.server.models.*;
import com.noroff.no.server.repositories.*;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import javax.annotation.security.RolesAllowed;
import java.net.URI;
import java.util.List;
import java.util.Random;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("api/v1/game")
@RestController
public class PlayerController {
    Logger logger = LoggerFactory.getLogger(PlayerController.class);
    private final PlayerRepository playerRepository;
    private final GameRepository gameRepository;
    private final KillRepository killRepository;
    private final UserRepository userRepository;
    private final SquadMemberRepository squadMemberRepository;

    public PlayerController(PlayerRepository playerRepository, GameRepository gameRepository, KillRepository killRepository, UserRepository userRepository, SquadMemberRepository squadMemberRepository) {
        this.playerRepository = playerRepository;
        this.gameRepository = gameRepository;
        this.killRepository = killRepository;
        this.userRepository = userRepository;
        this.squadMemberRepository = squadMemberRepository;
    }

    /**
     * This method is used to get all players in a game.
     *
     * @param gameId    is the id of the game the user wants to get a list of players.
     * @param principal is the Jwt for authentication. Players are not authorized to see all information about
     *                  other players.
     * @return message about there being no players in the game or a list of all the players in the game
     */
    @Operation(summary = "Get all players in a specific game.", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Player.class))),
            @ApiResponse(responseCode = "204", description = "No players registered in the game")
    })
    //Listing out the players in the game
    @GetMapping("/{gameId}/player")
    @RolesAllowed({"Admin, User"})
    public ResponseEntity<?> getAllPlayers(@Parameter(description = "Id of game to get players in")
                                           @PathVariable Long gameId,
                                           @AuthenticationPrincipal Jwt principal) {
        //Gets all the players in the database
        List<Player> playersInGame = playerRepository.findAllByGameGameId(gameId);
        if (playersInGame.size() == 0) {
            String message = String.format("There are no players in the game (%d)", gameId);
            return new ResponseEntity<>(message, HttpStatus.NO_CONTENT);
        }
        logger.info(String.format("GET game/%d/player from %s successful!", gameId, principal.getClaimAsString("name")));
        if (!principal.getClaimAsStringList("roles").contains("Admin")) {
            // If player is a user, it will obscure sensitive information of other players
            // Obscures:
            // BiteCode, Admin field, isHuman field
            for (Player player : playersInGame) {
                if (!player.getUser().getEmail().equals(principal.getClaimAsString("email"))) {
                    player.setBiteCode("Unavailable");
                    player.getUser().setAdmin(null);
                    player.setHuman(null);
                }
            }
        }
        return ResponseEntity.ok()
                .header("message", "Fetched Players successful!")
                .body(playersInGame);
    }

    /**
     * This method is used to get a specific player in a game.
     *
     * @param gameId    is the id of the game to get the player from
     * @param playerId  is the id of the player
     * @param principal is the Jwt for authentication.
     * @return 200 if GET request is successful. 400 if mismatch in information given, player is not in the game
     * 404 if player or game does not exist
     */
    @Operation(summary = "Get a player in a specific game.", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Player.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request. " +
                    "Given player id does not belong in given game id. "),
            @ApiResponse(responseCode = "404", description = "Game or player not found"),
    })
    @GetMapping("/{gameId}/player/{playerId}")
    @RolesAllowed({"Admin, User"})
    public ResponseEntity<?> getSpecific(@Parameter(description = "Id of the game the player to get is in")
                                         @PathVariable Long gameId,
                                         @Parameter(description = "Id of the player to get")
                                         @PathVariable Long playerId,
                                         @AuthenticationPrincipal Jwt principal) {
        if (!playerRepository.existsById(playerId)) {
            String message = String.format("Player id (%d) could not be found", playerId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        } else if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        } else if (!playerRepository.existsByGameGameIdAndPlayerId(gameId, playerId)) {
            String message = String.format("Player id (%d) could not be found in game (%d)", playerId, gameId);
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        Player returnPlayer = playerRepository.getOne(playerId);
        if (!principal.getClaimAsStringList("roles").contains("Admin")) {
            // If player is a user, it will obscure sensitive information of other players
            // Obscures:
            // BiteCode, Admin field, isHuman field

            if (!returnPlayer.getUser().getEmail().equals(principal.getClaimAsString("email"))) {
                returnPlayer.setBiteCode("Unavailable");
                returnPlayer.getUser().setAdmin(null);
                returnPlayer.setHuman(null);
            }
        }
        return ResponseEntity.ok()
                .header("message", "Fetched player successful!")
                .body(returnPlayer);
    }

    /**
     * This method is used to create a new player
     *
     * @param gameId    is the id of the game to create the new player in
     * @param player    is the player object to create.
     * @param principal is the Jwt for authentication. Admins are authorized to customize a new player.
     * @return 201 if player get created. 404 if game is not found.
     * 400 if Game is not in registration phase or the user already has a player in the game.
     */
    @Operation(summary = "Create a new player.", security = @SecurityRequirement(name = "Bearer-token"))
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Player to be posted.",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Player.class))},
            required = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "POST request successful"),
            @ApiResponse(responseCode = "400", description = "Bad Request. User already has a player in this game. " +
                    "Game already completed. " +
                    "Game id in path does not match game id in body. "),
            @ApiResponse(responseCode = "404", description = "Game not found"),
    })
    @PostMapping("/{gameId}/player")
    @RolesAllowed({"Admin, User"})
    public ResponseEntity<?> createPlayer(@Parameter(description = "Id of the game to create a player in")
                                          @PathVariable Long gameId,
                                          @RequestBody Player player,
                                          @AuthenticationPrincipal Jwt principal) {

        if (player.getGame() == null || player.getGame().getGameId() == null) {
            player.setGame(new Game(gameId));
        } else if (!player.getGame().getGameId().equals(gameId)) {
            String message = String.format("Game id (%d) in path does not match game id (%d) in body",
                    gameId, player.getGame().getGameId());
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        User user = userRepository.findByUsername(player.getUser().getUsername());
        player.setUser(user);
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game (%d) does not exist.", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        } else if (playerRepository.existsByGameGameIdAndUserUserId(gameId, player.getUser().getUserId())) {
            String message = "User already has a player in this game.";
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }

        Game game = gameRepository.getOne(gameId);
        if (game.getGameState().getStateId().equals(3L)) {
            String message = String.format("Game (%d) is already completed.", gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        // Create a biteCode
        String biteCode = randomStringGenerator();
        // Check if the newly created biteCode already exists in the game.
        boolean exist = playerRepository.existsByBiteCodeAndGameGameId(biteCode, gameId);
        while (exist) {
            biteCode = randomStringGenerator();
            if (!playerRepository.existsByBiteCodeAndGameGameId(biteCode, gameId)) {
                exist = false;
            }
        }
        player.setBiteCode(biteCode);
        if (!principal.getClaimAsStringList("roles").contains("Admin")) {
            player.setHuman(true);
            if (!game.getGameState().getStateId().equals(1L)) {
                String message = "User can only register while game is in registration state";
                return ResponseEntity.badRequest()
                        .header("message", message)
                        .body(message);
            }
        } else {
            if (player.getHuman() == null) {
                player.setHuman(true);
            }
        }
        Player returnPlayer = playerRepository.save(player);
        URI location = URI.create(String.format("/game/%d/player/%d", gameId, returnPlayer.getPlayerId()));
        return ResponseEntity.created(location)
                .header("message", "Player created successful!")
                .body(returnPlayer);
    }

    /**
     * This method is used to generate a random string as the biteCode.
     *
     * @return the biteCode string.
     */
    public String randomStringGenerator() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 7;
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }


    // TODO: Change so killer can update kill description, currently admin only

    /**
     * This method is used to update a player.
     *
     * @param gameId   is the id of the game where the player is
     * @param playerId is the id of the player to update
     * @param player   is the object with information to update.
     * @return 200 if update is successful. 404 if Game or Player not found.
     * 400 if mismatch in information.
     */
    @Operation(summary = "Update a player.", security = @SecurityRequirement(name = "Bearer-token"))
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Player to be updated.",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Player.class))},
            required = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "PUT request successful. Expected changes: State of " +
                    "player (zombie or human). "),
            @ApiResponse(responseCode = "400", description = "Bad Request. " +
                    "Given player id does not belong in given game id. " +
                    "Game id in body does not match game id in path. " +
                    "Game already completed"),
            @ApiResponse(responseCode = "404", description = "Game or player not found"),
    })
    @PutMapping("/{gameId}/player/{playerId}")
    @RolesAllowed("Admin")
    public ResponseEntity<?> updatePlayer(@Parameter(description = "Id of the game where the player to update is")
                                          @PathVariable Long gameId,
                                          @Parameter(description = "Id of the player to update")
                                          @PathVariable Long playerId,
                                          @RequestBody Player player) {
        if (player.getGame() == null || player.getGame().getGameId() == null) {
            player.setGame(new Game(gameId));
        } else if (!player.getGame().getGameId().equals(gameId)) {
            String message = String.format("Game id (%d) in path does not match game id (%d) for player",
                    gameId, player.getGame().getGameId());
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        if (player.getPlayerId() == null) {
            player.setPlayerId(playerId);
        } else if (!player.getPlayerId().equals(playerId)) {
            String message = String.format("Player id (%d) in path does not match player id (%d) in body",
                    playerId, player.getPlayerId());
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }

        if (!playerRepository.existsById(player.getPlayerId())) {
            String message = String.format("Player id (%d) could not be found.",
                    player.getPlayerId());
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        } else if (!gameRepository.existsById(player.getGame().getGameId())) {
            String message = String.format("Game id (%d) could not be found.",
                    player.getGame().getGameId());
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        Game game = gameRepository.getOne(gameId);
        if (game.getGameState().getStateId().equals(3L)) {
            String message = String.format("Game (%d) is already completed.", gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }

        Player oldPlayer = playerRepository.getOne(playerId);
        if (!oldPlayer.getGame().getGameId().equals(gameId)) {
            String message = String.format("Game id (%d) in path does not match the game id (%d) of the player",
                    gameId,
                    oldPlayer.getGame().getGameId());
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        if (player.getHuman() != null) {
            if (player.getHuman() && !oldPlayer.getHuman() && killRepository.existsByVictimPlayerId(playerId)) {
                Kill kill = killRepository.findByVictimPlayerId(playerId);
                killRepository.delete(kill);
            }
            if (!oldPlayer.getHuman() && player.getHuman() || oldPlayer.getHuman() && !player.getHuman()) {
                if (oldPlayer.squadMemberGetter() != null) {
                    if (oldPlayer.squadsGetter() != null) {
                        SquadMember squadMember = squadMemberRepository.getOne(oldPlayer.squadMemberGetter());
                        squadMember.setSquad(null);
                        squadMember.setActive(false);
                        squadMemberRepository.save(squadMember);
                    }
                }
            }
            oldPlayer.setHuman(player.getHuman());
        }


        Player returnPlayer = playerRepository.save(oldPlayer);
        return ResponseEntity.ok()
                .header("message", "Edit successful")
                .body(returnPlayer);

    }

    /**
     * This method is used to delete a player.
     *
     * @param gameId   is the id of the game where the player is.
     * @param playerId is the id of the player to delete
     * @return 200 if delete is successful. 400 if mismatch in information.
     * 404 if game or player could not be found
     */
    @Operation(summary = "Delete a player in a game.", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "DELETE request successful. Expected changes: " +
                    "Removal of given Player from the database."),
            @ApiResponse(responseCode = "400", description = "Bad Request. Game already completed. " +
                    "Given player id does not belong in given game id."),
            @ApiResponse(responseCode = "404", description = "Game or player not found"),
    })
    @DeleteMapping("/{gameId}/player/{playerId}")
    @RolesAllowed("Admin")
    public ResponseEntity<?> deletePlayer(@Parameter(description = "Id of the game where the player to delete is")
                                          @PathVariable Long gameId,
                                          @Parameter(description = "Id of the player to delete")
                                          @PathVariable Long playerId) {
        if (!playerRepository.existsById(playerId)) {
            String message = String.format("The player with id (%d) could not be found", playerId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        } else if (!gameRepository.existsById(gameId)) {
            String message = String.format("The game with id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }
        Game game = gameRepository.getOne(gameId);
        if (game.getGameState().getStateId().equals(3L)) {
            String message = String.format("Game (%d) is already completed.", gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        } else if (!playerRepository.existsByGameGameIdAndPlayerId(gameId, playerId)) {
            String message = String.format("The player with id (%d) does not belong in game (%d)", playerId, gameId);
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        if (killRepository.existsByVictimPlayerId(playerId)) {
            Kill kill = killRepository.findByVictimPlayerId(playerId);
            killRepository.delete(kill);
        }
        Player player = playerRepository.getOne(playerId);
        playerRepository.delete(player);
        return new ResponseEntity<>("Delete successful", HttpStatus.OK);
    }
}
