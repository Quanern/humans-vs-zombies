package com.noroff.no.server.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(name = "games")
public class Game {

    @Id
    @Column(name = "game_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long gameId;

    @Column(name = "name")
    private String name;

    @ManyToOne()
    @JoinColumn(name = "state_id", nullable = false)
    private GameState gameState;

    @Column(name = "north")
    private String north;

    @Column(name = "south")
    private String south;

    @Column(name = "west")
    private String west;

    @Column(name = "east")
    private String east;

    @Column(name = "description")
    private String description;

    @Column(name = "start")
    private Date start;

    @OneToMany(mappedBy = "game", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Chat> chats;

    @OneToMany(mappedBy = "game", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Kill> kills;

    @OneToMany(mappedBy = "game", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Player> players;

    @OneToMany(mappedBy = "game", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Mission> missions;

    @OneToMany(mappedBy = "game", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Squad> squads;

    @JsonGetter("players")
    public List<String> playersGetter() {
        if (players != null) {
            return players.stream().map(player -> {
                return player.getUser().getUsername();
            }).collect(Collectors.toList());
        }
        return null;
    }

    @JsonGetter("kills")
    public List<String> killsGetter() {
        return null;
    }
    public Game(){}

    public Game(Long gameId) {
        this.gameId = gameId;
    }

    @JsonGetter("chats")
    public List<String> chatsGetter() {
        return null;
    }

    @JsonGetter("squads")
    public List<Long> squadsGetter() {
        return null;
    }

    @JsonGetter("missions")
    public List<String> missionsGetter() {
        return null;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public String getNorth() {
        return north;
    }

    public void setNorth(String NWLng) {
        this.north = NWLng;
    }

    public String getSouth() {
        return south;
    }

    public void setSouth(String NWLat) {
        this.south = NWLat;
    }

    public String getWest() {
        return west;
    }

    public void setWest(String SELng) {
        this.west = SELng;
    }

    public String getEast() {
        return east;
    }

    public void setEast(String SELat) {
        this.east = SELat;
    }

    public Set<Kill> getKills() {
        return kills;
    }

    public void setKills(Set<Kill> kills) {
        this.kills = kills;
    }

    public Set<Player> getPlayers() {
        return players;
    }

    public void setPlayers(Set<Player> players) {
        this.players = players;
    }

    public Set<Chat> getChats() {
        return chats;
    }

    public void setChats(Set<Chat> chats) {
        this.chats = chats;
    }

    public Set<Mission> getMissions() {
        return missions;
    }

    public void setMissions(Set<Mission> missions) {
        this.missions = missions;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Set<Squad> getSquads() {
        return squads;
    }

    public void setSquads(Set<Squad> squads) {
        this.squads = squads;
    }
}
