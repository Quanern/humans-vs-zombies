package com.noroff.no.server.repositories;

import com.noroff.no.server.models.Game;
import com.noroff.no.server.models.Squad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SquadRepository extends JpaRepository<Squad, Long> {
    List<Squad> findAllByGameGameId(Long gameId);
    Boolean existsSquadBySquadIdAndGameGameId(Long squadId, Long gameId);
    Squad findSquadBySquadIdAndGameGameId(Long squadId, Long gameId);

    @Query(value = "SELECT MAX(squad_id)+1 AS new_id FROM squads", nativeQuery = true)
    Long generateNewSquadId();
}

