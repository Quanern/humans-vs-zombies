package com.noroff.no.server.models;

import javax.persistence.*;

@Entity
@Table(name="game_states")
public class GameState {
    @Id
    @Column(name = "state_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long stateId;

    @Column(name = "state")
    private String state;

    public GameState(Long stateId) {
        this.stateId = stateId;
    }
    public GameState(){}

    public Long getStateId() {
        return stateId;
    }

    public void setStateId(Long stateId) {
        this.stateId = stateId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
