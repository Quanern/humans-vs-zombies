package com.noroff.no.server.controller;

import com.noroff.no.server.models.*;
import com.noroff.no.server.repositories.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.net.URI;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("api/v1/game/{gameId}/squad")
@RestController
public class SquadCheckinController {
    private final SquadCheckinRepository squadCheckinRepository;
    private final SquadMemberRepository squadMemberRepository;
    private final PlayerRepository playerRepository;
    private final GameRepository gameRepository;
    private final SquadRepository squadRepository;

    public SquadCheckinController(SquadCheckinRepository squadCheckinRepository, SquadMemberRepository squadMemberRepository, PlayerRepository playerRepository, GameRepository gameRepository, SquadRepository squadRepository) {
        this.squadCheckinRepository = squadCheckinRepository;
        this.squadMemberRepository = squadMemberRepository;
        this.playerRepository = playerRepository;
        this.gameRepository = gameRepository;
        this.squadRepository = squadRepository;
    }

    /**
     * This method is used to get all checkins in a game.
     *
     * @param gameId  is the id of the game where the squad is.
     * @param squadId is the id of the squad.
     * @return 200 if checkin is successfully fetched. 204 if there are No Content
     * 400 if mismatch in information. 404 if game or squad could not be found.
     */
    @Operation(summary = "Get all checkins in a squad for a player.", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = SquadCheckin.class))),
            @ApiResponse(responseCode = "204", description = "No checkins for the squad"),
            @ApiResponse(responseCode = "400", description = "Bad Request. " +
                    "Given squad id does not belong in given game id. " +
                    "Given player id is not in a squad. " +
                    "Given player can not see into another faction's checkins. " +
                    "Given player does not belong in given squad. "),
            @ApiResponse(responseCode = "404", description = "Game or Squad not found"),
    })
    @GetMapping("/{squadId}/checkin/{playerId}")
    @RolesAllowed({"Admin", "User"})
    public ResponseEntity<?> getCheckins(@Parameter(description = "Id of the game the squad is in")
                                         @PathVariable(value = "gameId") Long gameId,
                                         @Parameter(description = "Id of the squad to get checkins from")
                                         @PathVariable(value = "squadId") Long squadId,
                                         @Parameter(description = "Id of the squad to get checkins from")
                                         @PathVariable(value = "playerId") Long playerId) {
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        } else if (!squadRepository.existsById(squadId)) {
            String message = String.format("Squad id (%d) could not be found", squadId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        } else if (!squadRepository.existsSquadBySquadIdAndGameGameId(squadId, gameId)) {
            String message = String.format("Squad id (%d) does not belong to game (%d)", squadId, gameId);
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        } else if (!playerRepository.existsById(playerId)) {
            String message = String.format("Player id (%d) could not be found", playerId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }
        Player player = playerRepository.getOne(playerId);
        if (player.squadMemberGetter() == null) {
            String message = "Player is not in a squad";
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        Squad squad = squadRepository.getOne(squadId);
        SquadMember squadMember = squadMemberRepository.getOne(player.squadMemberGetter());

        if (squad.getHuman() && !player.getHuman() || player.getHuman() && !squad.getHuman()) {
            String message = "Player can not see enemy faction's checkins";
            return ResponseEntity.status(HttpStatus.FORBIDDEN)
                    .header("message", message)
                    .body(message);
        }
        if (!squadMember.getSquad().getSquadId().equals(squadId)) {
            String message = String.format("Player (%d) is not in squad (%d)", playerId, squadId);
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        List<SquadCheckin> checkins = squadCheckinRepository.getLatestCheckinBySquadId(squadId);
        checkins.removeIf(sqc -> !sqc.getSquadMember().getActive());
        if (checkins.size() == 0) {
            String message = String.format("No squad checkins in game (%d)", gameId);
            return new ResponseEntity<>(message, HttpStatus.NO_CONTENT);

        }
        return ResponseEntity.ok()
                .header("message", "Fetched checkins successful!")
                .body(checkins);
    }

    /**
     * Method for admin to get all checkins in a game
     * @param gameId is the id of the game admin wants checkins for
     * @return 200 a list of all checkins. 204 no content. 404 game not found.
     */

    @Operation(summary = "Get all checkins in a game for admin.", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = SquadCheckin.class))),
            @ApiResponse(responseCode = "204", description = "No checkins for the game"),
            @ApiResponse(responseCode = "404", description = "Game not found"),
    })
    @GetMapping("/checkin")
    @RolesAllowed({"Admin"})
    public ResponseEntity<?> getCheckins(@Parameter(description = "Id of the game the squad is in")
                                         @PathVariable(value = "gameId") Long gameId) {
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        List<SquadCheckin> checkins = squadCheckinRepository.getLatestCheckinByGameId(gameId);
        checkins.removeIf(sqc -> !sqc.getSquadMember().getActive());
        if (checkins.size() == 0) {
            String message = String.format("No squad checkins in game (%d)", gameId);
            return new ResponseEntity<>(message, HttpStatus.NO_CONTENT);

        }
        return ResponseEntity.ok()
                .header("message", "Fetched checkins successful!")
                .body(checkins);
    }

    /**
     * This method is used to create a new checkin for a squadMember.
     *
     * @param gameId       is the id of the game where the checkin happens.
     * @param squadId      is the id of the squad to checkin for.
     * @param squadCheckin is the object with information for the post.
     * @return 200 if checkin is successfully created. 400 if mismatch in information.
     * 404 if player, squad, game or squadMember not found.
     */
    @Operation(summary = "Create a new checkin.", security = @SecurityRequirement(name = "Bearer-token"))
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Checkin to be posted.",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = SquadCheckin.class))},
            required = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "POST request successful"),
            @ApiResponse(responseCode = "400", description = "Bad Request. " +
                    "Given squad does not belong in given game. " +
                    "No given player in the request body. " +
                    "Given player does not belong in given squad. " +
                    "Given player does not belong in given game. " +
                    "Given squad does not belong in given game. " +
                    "Given player is not a squad member. " +
                    "Game already completed. "),
            @ApiResponse(responseCode = "404", description = "Game, player, squad not found"),
    })
    @PostMapping("/{squadId}/checkin")
    @RolesAllowed({"Admin", "User"})
    public ResponseEntity<?> checkIn(@Parameter(description = "Id of the game the squad is in")
                                     @PathVariable(value = "gameId") Long gameId,
                                     @Parameter(description = "Id of the squad to create a checkin for")
                                     @PathVariable(value = "squadId") Long squadId,
                                     @RequestBody SquadCheckin squadCheckin) {

        Long playerId = squadCheckin.getSquadMember().getPlayer().getPlayerId();
        if (playerId == null) {
            String message = "There must be a valid player id in the request";
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        if (!playerRepository.existsById(playerId)) {
            String message = String.format("Player id(%d) could not be found", playerId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        Player player = playerRepository.getOne(playerId);
        squadCheckin.setSquad(new Squad(squadId));
        if (player.getGame() != null) {
            squadCheckin.setGame(player.getGame());
        } else {
            squadCheckin.setGame(new Game(gameId));
        }
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }
        Game game = gameRepository.getOne(gameId);
        if (game.getGameState().getStateId().equals(3L)) {
            String message = String.format("Game (%d) is already completed.", gameId);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }

        if (!squadRepository.existsById(squadId)) {
            String message = String.format("Squad id (%d) could not be found", squadId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        } else if (!squadCheckin.getGame().getGameId().equals(gameId)) {
            String message = String.format("Game id (%d) in path does not match game id (%d) for player",
                    gameId, player.getGame().getGameId());
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        } else if (!squadRepository.existsSquadBySquadIdAndGameGameId(squadId, gameId)) {
            String message = String.format("Squad id(%d) does not belong in game(%d)", squadId, gameId);
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        }
        if (!playerRepository.existsByGameGameIdAndPlayerId(gameId, player.getPlayerId())) {
            String message = String.format("Player id (%d) does not belong in game (%d)",
                    player.getPlayerId(),
                    gameId);
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);

        } else if (player.squadMemberGetter() == null) {
            String message = "Player is not in a squad";
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        } else if (!squadMemberRepository.existsById(player.squadMemberGetter())) {
            String message = String.format("Squad Member id (%d) could not be found",
                    squadCheckin.getSquadMember().getSquadMemberId());
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
        }
        SquadMember squadMember = squadMemberRepository.getOne(player.squadMemberGetter());
        squadCheckin.setSquadMember(squadMember);
        squadCheckin.setTime(new Date());
        SquadCheckin returnSquadCheckin = squadCheckinRepository.save(squadCheckin);
        URI location = URI.create(String.format("/game/%d/checkin", gameId));
        return ResponseEntity.created(location)
                .header("message", "Checkin added successful!")
                .body(returnSquadCheckin);
    }
}
