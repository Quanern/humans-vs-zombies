package com.noroff.no.server.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="chats")
public class Chat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "message_id")
    private Long messageId;

    @Column(name = "message")
    private String message;

    @Column(name = "human_global")
    private Boolean humanGlobal;

    @Column(name = "zombie_global")
    private Boolean zombieGlobal;

    @Column(name = "timestamp")
    private Date timestamp;

    @ManyToOne
    @JoinColumn(name = "game_id", nullable = false)
    private Game game;

    @ManyToOne
    @JoinColumn(name = "player_id")
    private Player player;

    @ManyToOne
    @JoinColumn(name = "squad_id")
    private Squad squad;

    @JsonGetter("game")
    public Long gameGetter() {
        if(game == null || game.getGameId() == null){
            return null;
        }
        return game.getGameId();
    }

    @JsonGetter("player")
    public Long playerGetter(){
        if(player == null){
            return null;
        }
        return player.getPlayerId();
    }

    @JsonGetter("user")
    public String userGetter(){
        if(player.getUser() == null || player.getUser().getUsername() == null){
            return null;
        }
        return player.getUser().getUsername();
    }
    @JsonGetter("squad")
    public Long squadGetter(){
        if(squad == null){
            return null;
        }
        return squad.getSquadId();
    }

    @JsonSetter("game")
    public void gameSetter(Long gameId){
        this.setGame(new Game(gameId));
    }

    @JsonSetter("squad")
    public void squadSetter(Long squadId){
        this.setSquad(new Squad(squadId));
    }

    @JsonSetter("player")
    public void playerSetter(Long playerId){
        this.setPlayer(new Player(playerId));
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getHumanGlobal() {
        return humanGlobal;
    }

    public void setHumanGlobal(Boolean humanGlobal) {
        this.humanGlobal = humanGlobal;
    }

    public Boolean getZombieGlobal() {
        return zombieGlobal;
    }

    public void setZombieGlobal(Boolean zombieGlobal) {
        this.zombieGlobal = zombieGlobal;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game gameId) {
        this.game = gameId;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Squad getSquad() {
        return squad;
    }

    public void setSquad(Squad squadId) {
        this.squad = squadId;
    }
}
