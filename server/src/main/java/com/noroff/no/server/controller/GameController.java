package com.noroff.no.server.controller;

import com.noroff.no.server.models.Game;
import com.noroff.no.server.models.GameState;
import com.noroff.no.server.models.Player;
import com.noroff.no.server.models.SquadMember;
import com.noroff.no.server.repositories.GameRepository;

import com.noroff.no.server.repositories.SquadMemberRepository;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.noroff.no.server.repositories.PlayerRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Random;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("api/v1/game")
@RestController
public class GameController {
    Logger logger = LoggerFactory.getLogger(GameController.class);
    private final GameRepository gameRepository;
    private final PlayerRepository playerRepository;
    private final SquadMemberRepository squadMemberRepository;

    public GameController(GameRepository gameRepository, PlayerRepository playerRepository, SquadMemberRepository squadMemberRepository) {
        this.gameRepository = gameRepository;
        this.playerRepository = playerRepository;
        this.squadMemberRepository = squadMemberRepository;
    }

    /**
     * Get all the games in the database.
     *
     * @return 204 No Content if there are no games found. 200 OK if the games are fetched and a list of games.
     */
    @Operation(summary = "Get all games")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Game.class))}),
            @ApiResponse(responseCode = "204", description = "No content")})
    @GetMapping()
    public ResponseEntity<?> getAllGames(){

        // Gets all the games in the database.
        List<Game> games = gameRepository.findAll();
        // If there are no games in the database, return no content.
        if (games.size() == 0) {
            String message = "No games found";
            return new ResponseEntity<>(message, HttpStatus.NO_CONTENT);

        }
        logger.info("GET game/ successful");
        return ResponseEntity.ok()
                .header("message", "Games fetched successful")
                .body(games);
    }

    /**
     * This method is used to get a specific game in the database.
     * @param gameId is the id of the game you want information about.
     * @param principal is the Jwt  needed for authentication. Admins and registered users are authorized.
     * @return 404 if game could not be found. 200 if game was fetched successfully.
     */
    @Operation(summary = "Get a specific game based on game Id", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "GET request successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Game.class))}),
            @ApiResponse(responseCode = "404", description = "Game not found")})
    @GetMapping("/{gameId}")
    @RolesAllowed({"Admin", "User"})
    public ResponseEntity<?> getGame(@Parameter(description = "Id of the game to get")
                                     @PathVariable(value = "gameId") Long gameId,
                                     @AuthenticationPrincipal Jwt principal) {
        String logMsg = String.format("GET getGame(%d) called by %s ", gameId, principal.getClaimAsString("name"));
        logger.info(logMsg);

        // Checks if game exists, if it doesn't, return not found.
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game with id (%d) could not be found", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        Game game = gameRepository.getOne(gameId);
        return ResponseEntity.ok()
                .header("message", "Game fetched successful")
                .body(game);
    }

    /**
     * This method is used to create a game
     *
     * @param game is the game object that is to be created
     * @param principal is the Jwt needed for authentication. Only admin is authorized.
     * @return 201 Created if the Game got posted. Returns the chat object.
     */
    @Operation(summary = "Create a new game.", security = @SecurityRequirement(name = "Bearer-token"))
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Information about the game to be created",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Game.class))},
            required = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "POST request successful",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Game.class))})})
    @PostMapping()
    @RolesAllowed("Admin")
    public ResponseEntity<?> createGame(@RequestBody Game game,
                                        @AuthenticationPrincipal Jwt principal) {
        game.setGameState(new GameState(1L));
        game.setStart(new Date());
        Game returnGame = gameRepository.save(game);
        URI location = URI.create("/game");
        return ResponseEntity.created(location)
                .header("message", "Game added successful!")
                .body(returnGame);
    }

    //TODO: Add coordinates check

    /**
     * This method is used to update a game
     *
     * @param game   is the object to replace the old one
     * @param gameId is the id of the game to be updated
     * @param principal is the Jwt needed for authentication. Only admin is authorized.
     * @return 400 if the game Id in the path doesn't match game Id in game.
     * 404 if the game Id could not be found.
     * 200 if the update went through
     */
    @Operation(summary = "Update a game.", security = @SecurityRequirement(name = "Bearer-token"))
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Information about the game to be updated",
            content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = Game.class))},
            required = true)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "PUT request successful. Expected changes: " +
                    "Location of the game, name of the game or state of the game. "),
            @ApiResponse(responseCode = "400", description = "Bad Request, Id in path and body are different"),
            @ApiResponse(responseCode = "404", description = "Game not found")})
    @PutMapping("/{gameId}")
    @RolesAllowed("Admin")
    public ResponseEntity<?> updateGame(@RequestBody Game game,
                                        @Parameter(description = "Id of the game to update")
                                        @PathVariable(value = "gameId") Long gameId,
                                        @AuthenticationPrincipal Jwt principal){
        String logMsg = String.format("PUT createGame(%d) called by %s", gameId, principal.getClaimAsString("name"));
        logger.info(logMsg);
        // Checks if the gameId in the path is the same as in the request body. Checks if the game exist or not as well.
        if (game.getGameId() == null) {
            game.setGameId(gameId);
        }
        if (!game.getGameId().equals(gameId)) {
            String message = String.format("Game id in path (%d) does not match game id in body (%d)",
                    gameId, game.getGameId());
            return ResponseEntity.badRequest()
                    .header("message", message)
                    .body(message);
        } else if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game with id (%d) could not be found.", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        Game oldGame = gameRepository.getOne(gameId);
        if (game.getName() != null) {
            oldGame.setName(game.getName());
        }
        if (game.getGameState() != null) {
            // If the previous game state was registration and the new is ongoing, create a patient zero.
            if(game.getGameState().getStateId().equals(2L) && oldGame.getGameState().getStateId().equals(1L)){
                int countZombies = 0;
                // Turns the set of players into an array
                Player[] players = oldGame.getPlayers().toArray(new Player[0]);
                if(players.length < 2){
                    String message = "There must be at least 2 players in the game.";
                    return ResponseEntity.badRequest()
                            .header("message", message)
                            .body(message);
                }
                // Initialize a random number from 0 to size of array -1.
                Random random = new Random();
                int randomNumber = random.nextInt(players.length-1);

                // Checks if there are already zombies in the game
                for (Player player: players)
                    if(!player.getHuman()){
                        countZombies++;
                }
                // If there are no zombies in the game
                if(countZombies == 0) {
                    // Gets a random player from the array.
                    Player patientZero = players[randomNumber];
                    // Fetches the player's data from the database.
                    patientZero = playerRepository.getOne(patientZero.getPlayerId());
                    // Sets the faction to zombie and update it in the database.
                    patientZero.setHuman(false);
                    if (patientZero.squadMemberGetter() != null) {
                        SquadMember squadMember = squadMemberRepository.getOne(patientZero.squadMemberGetter());
                        squadMember.setActive(false);
                        squadMemberRepository.save(squadMember);
                    }
                    playerRepository.save(patientZero);
                }
            }
            oldGame.setGameState(game.getGameState());

        }
        if (game.getDescription() != null) {
            oldGame.setDescription(game.getDescription());
        }
        if (game.getEast() != null){
            oldGame.setEast(game.getEast());
        }

        if (game.getWest() != null){
            oldGame.setWest(game.getWest());
        }

        if (game.getSouth() != null){
            oldGame.setSouth(game.getSouth());
        }

        if (game.getNorth() != null){
            oldGame.setNorth(game.getNorth());
        }

        Game newGame = gameRepository.save(oldGame);
        return ResponseEntity.ok()
                .header("message", "Edit game successful!")
                .body(newGame);
    }



    /**
     * This method is used to delete a game
     *
     * @param gameId is the Id of the game to be deleted
     * @param principal is the Jwt needed for authentication. Only admins are authorized.
     * @return 200 if the delete was successful. 404 if the game could not be found.
     */
    @Operation(summary = "Delete a game.", security = @SecurityRequirement(name = "Bearer-token"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "DELETE request successful. Expected change: " +
                    "Removal of the game from the database."),
            @ApiResponse(responseCode = "404", description = "Game not found")})
    @DeleteMapping("/{gameId}")
    @RolesAllowed("Admin")
    public ResponseEntity<?> deleteGame(@Parameter(description = "Id of the game to delete")
                                        @PathVariable(value = "gameId") Long gameId,
                                        @AuthenticationPrincipal Jwt principal){
        String logMsg = String.format("DELETE deleteGame(%d) called by %s", gameId, principal.getClaimAsString("name"));
        logger.info(logMsg);
        // Checks if game exists, if it doesn't, then returns a not found.
        if (!gameRepository.existsById(gameId)) {
            String message = String.format("Game with id (%d) does not exist", gameId);
            return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);

        }
        Game returnGame = gameRepository.getOne(gameId);
        gameRepository.delete(returnGame);
        return new ResponseEntity<>("Delete successful!", HttpStatus.OK);
    }
}
