import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import UserService from "./Services/UserService";

const renderApp = () => ReactDOM.render(<App/>, document.getElementById("root"));
UserService.initKeycloak(renderApp);
