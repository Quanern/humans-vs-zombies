import Keycloak from "keycloak-js";
import {URI} from "../Constants/uri";

const _kc = new Keycloak('/keycloak.json');

/**
 * Initializes Keycloak instance and calls the provided callback function if successfully authenticated.
 *
 * @param renderCallback
 */
const initKeycloak = (renderCallback) => {
    _kc.init({
        onLoad: 'check-sso',
        silentCheckSsoRedirectUri: window.location.origin + '/silent-check-sso.html',
        pkceMethod: 'S256'
    })

        .then(() => {
            renderCallback();
        })
};

const doLogin = () => _kc.login({
    redirectUri: URI.Client.Default + '/login'
});

const doLogout = () => _kc.logout({
    redirectUri: URI.Client.Default
});

const getToken = () => _kc.token;

const isLoggedIn = () => !!_kc.token;

const updateToken = (successCallback) =>
    _kc.updateToken(5)
        .then(successCallback)
        .catch(doLogin);

const getUsername = () => _kc.tokenParsed?.preferred_username;

const getFirstName = () => _kc.tokenParsed?.given_name;

const hasRole = (roles) => roles.some((role) =>_kc.hasResourceRole(role));

const UserService = {
    initKeycloak,
    doLogin,
    doLogout,
    isLoggedIn,
    getToken,
    updateToken,
    getUsername,
    getFirstName,
    hasRole,
};

export default UserService;
