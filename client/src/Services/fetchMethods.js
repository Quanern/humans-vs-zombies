import {URI} from "../Constants/uri";
import UserService from "./UserService";

// CTRL + R for find and replace .Production' to 'Production'
const authHeader = () => {
    return {
        headers:{
            'Authorization': `Bearer ${UserService.getToken()}`
        }
    }
}

export const postInit = (postBody) => {
    return {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            ...authHeader().headers
        },
        body: JSON.stringify(postBody)
    }
}
const putInit = (putBody) => {
    return {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            ...authHeader().headers
        },
        body: JSON.stringify(putBody)
    }
}

const deleteInit = () => {
    return {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            ...authHeader().headers
        },
    }
}

const handleInitialResponse = async (res, refreshCallback, bHandleResponse) => {
    if (res.status === 401) {
        console.log("401 in squadCheckin(), refreshing token")
        return UserService.updateToken(() => {refreshCallback()})
    }
    else if (res.status === 404)
    {
        throw new Error("404");
    }
    else if (!res.ok) {
        throw new Error(await res.text());
    }
    else if (res.status === 204){
        return null;
    }
    if (bHandleResponse) return await res.json();
}

//adding comment
//Game
export async function getGames()
{
    const response = await fetch(URI.ResourceServer.Default + "/api/v1/game/")
    return await response.json()
}

export async function getSpecificGame(gameId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}`, authHeader())
            .then((res) => handleInitialResponse(res, fetchRequest, true));
    }
    return fetchRequest()
}

export async function addGame(game) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/`, postInit(game))
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}

export async function updateGame(game) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${game.gameId}`, putInit(game))
            .then((res) => handleInitialResponse(res, fetchRequest, false))
    }
    return fetchRequest()
}

export async function deleteGame(gameId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}`, deleteInit())
            .then((res) => handleInitialResponse(res, fetchRequest, false))
    }
    return fetchRequest()
}

//Players

export async function getPlayers(gameId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/player`, authHeader())
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}
export async function getSpecificPlayer(gameId, playerId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/player/${playerId}`, authHeader())
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}
//TODO: Is player the right to post??? The body contains a user
export async function addPlayer(gameId, player) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/player`, postInit(player))
            .then((res) => handleInitialResponse(res, fetchRequest, false))
    }
    return fetchRequest()

}
export async function updatePlayer(gameId, playerId, player) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/player/${playerId}`, putInit(player))
            .then((res) => handleInitialResponse(res, fetchRequest, false))
    }
    return fetchRequest()
}

export async function deletePlayer(gameId, playerId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/player/${playerId}`, deleteInit())
            .then((res) => handleInitialResponse(res, fetchRequest, false))
    }
    return fetchRequest()
}

// Squads

export async function getSquads(gameId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/squad`, authHeader())
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}
export async function getSpecificSquad(gameId, squadId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/squad/${squadId}`, authHeader())
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}
export async function addSquad(gameId, squad) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/squad`, postInit(squad))
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}
export async function updateSquad(gameId, squadId, squad) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/squad/${squadId}`, putInit(squad))
            .then((res) => handleInitialResponse(res, fetchRequest, false))
    }
    return fetchRequest()
}

export async function deleteSquad(gameId, squadId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/squad/${squadId}`, deleteInit())
            .then((res) => handleInitialResponse(res, fetchRequest, false))
    }
    return fetchRequest()
}


//Missions

export async function getMissions(gameId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/mission`, authHeader())
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}
export async function getSpecificMissions(gameId, missionId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/squad/${missionId}`, authHeader())
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}
export async function addMission(gameId, mission) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/mission`, postInit(mission))
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}
export async function updateMission(mission) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${mission.game}/mission/${mission.missionId}`, putInit(mission))
            .then((res) => handleInitialResponse(res, fetchRequest, false))
    }
    return fetchRequest()
}

export async function deleteMission(gameId, missionId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/mission/${missionId}`, deleteInit())
            .then((res) => handleInitialResponse(res, fetchRequest, false))
    }
    return fetchRequest()
}


// Squad checkin

export async function getCheckIn(gameId, squadId, playerId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/squad/${squadId}/checkin/${playerId}`, authHeader())
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}

export async function getAdminCheckIn(gameId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/squad/checkin`, authHeader())
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}

export async function squadCheckin(gameId, squadId, checkin) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/squad/${squadId}/checkin`, postInit(checkin))
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}

export async function squadCheckout(gameId, squadId, checkout) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/squad/${squadId}/checkout`, putInit(checkout))
            .then((res) => handleInitialResponse(res, fetchRequest, false))
    }
    return fetchRequest()
}


// Chat

export async function getChat(gameId, playerId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/chat/${playerId}`, authHeader())
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}

export async function getAdminChat(gameId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/chat`, authHeader())
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}

export async function getChatSquad(gameId, squadId, playerId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/squad/${squadId}/chat/${playerId}`, authHeader())
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}

export async function addChat(gameId, chat) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/chat`, postInit(chat))
            .then((res) => handleInitialResponse(res, fetchRequest, true))
            .catch(error => console.error(error))
    }
    return fetchRequest()
}
export async function addChatSquad(gameId, squadId, chat) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/squad/${squadId}/chat`, postInit(chat))
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}


// Squad member
export async function addSquadMember(gameId, squadId, player) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/squad/${squadId}/join`, postInit(player))
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}
//TODO: Player as param ok??
export async function leaveSquad(gameId, squadId, player) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/squad/${squadId}/leave`, putInit(player))
            .then((res) => handleInitialResponse(res, fetchRequest, false))
    }
    return fetchRequest()
}



// Kill
export async function getKillsInGame(gameId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/kill`, authHeader())
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}
export async function getKill(gameId, killId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/kill/${killId}`, authHeader())
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}
export async function addKill(gameId, kill) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/kill`, postInit(kill))
            .then((res) => handleInitialResponse(res, fetchRequest, true))
    }
    return fetchRequest()
}

// TODO: Need update??

export async function deleteKill(gameId, killId) {
    const fetchRequest = () => {
        return fetch(`${URI.ResourceServer.Default}/api/v1/game/${gameId}/kill/${killId}`, deleteInit())
            .then((res) => handleInitialResponse(res, fetchRequest, false))
    }
    return fetchRequest()
}
