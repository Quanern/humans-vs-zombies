export const browserStorage =
    {
        get(key)
        {
            const stored = localStorage.getItem(key);
            if (!stored) return null;
            else return JSON.parse(stored);
        },
        set(key, value)
        {
            try
            {
                const json = JSON.stringify(value);
                localStorage.setItem(key, json);
                return key;
            }
            catch (e)
            {
                console.log(e.message());
            }
        },
        clear()
        {
            localStorage.clear();
        }
    }
