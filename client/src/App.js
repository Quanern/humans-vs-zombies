import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter,
  Switch
} from 'react-router-dom';
import PublicRoute from "./Components/HOC/PublicRoute";
import {AppRoutes} from "./Constants/AppRoutes";
import LandingPage from "./Components/LandingPage/LandingPage";
import NotFound from "./Components/NotFound/NotFound";
import AdminPage from "./Components/AdminPage/AdminPage";
import NavigationBar from "./Components/Navbar/NavigationBar";
import GameDetailPage from "./Components/GameDetailPage/GameDetailPage";
import RolesRoute from "./Components/HOC/RolesRoute";
import Login from "./Components/LoginPage/Login";

function App() {

  return (
      <BrowserRouter>
        <div className="App">
            <NavigationBar/>
          <Switch>
            <PublicRoute path={AppRoutes.LandingPage} exact component={LandingPage}/>
            <RolesRoute roles={['Admin']} path={AppRoutes.AdminPage} component={AdminPage}/>
            <RolesRoute roles={['Admin', 'User']} path={AppRoutes.GameDetails} component={GameDetailPage}/>
            <RolesRoute roles={['Admin', 'User']} path={AppRoutes.Login} component={Login}/>
            <PublicRoute path={'*'} component={NotFound}/>
          </Switch>
        </div>
      </BrowserRouter>
  );
}


export default App;
