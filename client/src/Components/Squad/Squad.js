import './Squad.css';
import {useEffect, useState} from "react";
import SquadForm from "./SquadForm";
import SquadList from "./SquadList";
import {getSquads} from "../../Services/fetchMethods";

function Squad(props){

    const [squads, setSquads] = useState([])

    useEffect(() => {
        const fetchSquads = async () => {
            if (!props.game) return;
            const fetched = await getSquads(props.game.gameId)
            setSquads(fetched);
        }
        fetchSquads()
    }, [props.game])

    const handleSquad = (sq) => {
        if (squads) setSquads(old => [...old, {name: sq.name, numOfMembers: 0, deceased: 0}]);
        else setSquads([{name: sq.name, numOfMembers: 0, deceased: 0}]);
    }

    return (
        <div className="squad-container">
            <div className="squad-elements">
                <h3>Squads</h3>
                {props.role === "Player" &&
                <div>
                    <h4>Register Squad</h4>
                    <SquadForm player={props.player} game={props.game} squad = {handleSquad}/>
                </div>
                }
                <div>
                    <SquadList player={props.player} role={props.role} game={props.game} squads = {squads}/>
                </div>
            </div>
        </div>
    )
}
export default Squad;
