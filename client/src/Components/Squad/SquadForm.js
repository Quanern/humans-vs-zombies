import {Button, Form} from "react-bootstrap";
import {useState} from "react";
import {addSquad} from "../../Services/fetchMethods";

function SquadForm(props){
    const [squad, setSquad] = useState('');
    const [validated, setValidated] = useState(false);


    const handleChange = (e) => {
        setSquad(e.target.value);
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        const form = e.currentTarget;
        if (form.checkValidity() === false) {
            e.preventDefault();
            e.stopPropagation();
        } else {
            const squadObject = {name: squad, game: props.game.gameId, human: props.player.human, player: { playerId: props.player.playerId} }
                addSquad(props.game.gameId, squadObject).then(s => {
                    props.squad(squadObject)
                    window.location.reload(false);
                    console.log(s);
            });
        }
        setValidated(true);
    }
    return(
        <Form onSubmit={handleSubmit} validated={validated}>
            <Form.Group className="squad-form">
                <Form.Control type="text" placeholder="Write squad name..." className="squad-input" onChange={handleChange} required />
                <Button variant="primary" type="submit" className="squad-input-btn">Register</Button>
            </Form.Group>
        </Form>
    )
}
export default SquadForm;
