import {Button, Table} from "react-bootstrap";
import {useEffect, useState} from "react";
import {addSquadMember} from "../../Services/fetchMethods";

function SquadList(props){
    const squads = props.squads || [];
    const [squadJoined, setSquadJoined] = useState([])

    useEffect(() => {
        let result = squads.map(function(squad) {
            let o = Object.assign({}, squad);
            o.joined = false;
            return o;
        })
        setSquadJoined(result)
    },[props.squads])

    const click = async (sq) => {
        const squadMemberObj = {playerId: props.player.playerId};
        try {
            addSquadMember(sq.game, sq.squadId, squadMemberObj).then(() => {
                let result = squadJoined.map((squad) => {
                    if (sq.squadId === squad.squadId){
                        squad.joined = true
                        return squad
                    } else {
                        squad.joined = false
                        return squad
                    }
                })
                setSquadJoined(result);
                window.location.reload(false);
            })
        }catch (e) {
            alert(e.message)
        }
    }

    const listItems = () => {
        if (!props.squads) return;
        return squadJoined.filter(squad => {
            if (props.role === "Unregistered" || props.role === "Admin")
                return true;
            else {
                if (props.player)
                    return Boolean(squad.human) === Boolean(props.player.human);
                else return false;
            } }).map((squad, index) => {
            let squadMemberCount = 0;
            if(squad.squadMembers){
                squadMemberCount = squad.squadMembers.length;
            }
            return (
                <tr key={index} className="table-elements" data-item={squad}>
                    <td className="squad-name">{squad.name}</td>
                    <td>{squadMemberCount}</td>
                    <td>{squad.deceased}</td>
                    {(props.role === "Player") && props.game && props.game.gameState.stateId !== 3 &&
                    <td>
                        <Button variant="success" onClick={() => click(squad)} disabled={squad.joined}>
                            {!squad.joined ? 'Join' : 'Joined'}
                        </Button>
                    </td>
                    }
                </tr>
            )
        })
    }

    return (
        <div className="table-wrapper">
            <Table className="squad-table">
                <thead>
                <tr>
                    <th>Squad</th>
                    <th>Members</th>
                    <th>Dead</th>
                    {(props.role === "Player") && props.game && props.game.gameState.stateId !== 3 && <th/>}
                </tr>
                </thead>
                <tbody>
                    {listItems()}
                </tbody>
            </Table>
        </div>
    )
}
export default SquadList;
