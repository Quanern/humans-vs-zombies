import {ListGroup} from "react-bootstrap";
import {useEffect, useRef} from "react";

function MessageList(props) {

    useEffect(() => {
        scrollToBottom()
    }, [props.messages]);


    const messageEnd = useRef(null)

    const scrollToBottom = () => {
        messageEnd.current?.scrollIntoView({behavior: "smooth"})
    }

    const listItems = () => {
        return props.messages.map((message, index) => {
            let chatMode;
            if(message.humanGlobal && message.zombieGlobal){
                chatMode = "GLOBAL"
            } else if(message.squad != null){
                chatMode = "SQUAD"
            } else {
                chatMode = "FACTION"
            }
            return (
                <div>
                    <ListGroup.Item key={index} className="list-items">
                        <p className="player-chat"> [{chatMode}] {message.user}</p>
                        <p className={"text-chat"}>{message.message}</p>
                    </ListGroup.Item>
                    <div ref={messageEnd}/>
                </div>

            )
        })
    }

    return (
            <ListGroup className="list">
                {listItems()}
            </ListGroup>
    )
}

export default MessageList;
