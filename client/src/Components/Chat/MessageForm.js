import {Button, Form} from "react-bootstrap";
import {useState} from "react";
import {addChat, addChatSquad} from "../../Services/fetchMethods";

function MessageForm(props) {
    const [message, setMessage] = useState('');

    const handleChange = (e) => {
        setMessage(e.target.value);
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        const messageObject = {message: message.trim(), player: props.player.playerId}
        if (props.chatChosed === 'Global'){
            messageObject.humanGlobal = true
            messageObject.zombieGlobal = true
            addChat(props.gameId, messageObject)
        } else if (props.chatChosed === 'Squad') {
            addChatSquad(props.gameId, props.player.currentSquad, messageObject)
        } else {
            addChat(props.gameId, messageObject)
        }
        setMessage('')
    }
    return (
        <Form className="msg-form" onSubmit={handleSubmit}>
            <Form.Group controlId="message.input" className="chat-form">
                <Form.Control type="text" placeholder="Write message..." className="input-message" required onChange={handleChange} value={message} maxLength="100"/>
                <Button variant="customize" type="submit" className="btn-submit">Send</Button>
            </Form.Group>
        </Form>
    )
}
export default MessageForm;
