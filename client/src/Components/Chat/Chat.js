import MessageForm from "./MessageForm";
import MessageList from "./MessageList";
import './Chat.css'
import {useEffect, useRef, useState} from "react";
import {Button, ButtonGroup, ButtonToolbar} from "react-bootstrap";
import {getAdminChat, getChat, getChatSquad} from "../../Services/fetchMethods";
import UserService from "../../Services/UserService";

function Chat(props) {

    const chatOptionsInSquad = [{name: 'Global', btn: 'group'},
        {name: 'Faction', btn: 'group'},
        {name: 'Squad', btn: 'group'}]
    const chatOptionsNoSquad = [{name: 'Global', btn: 'group'},
        {name: 'Faction', btn: 'group'}]

    const [messages, setMessages] = useState([])
    const [message, setMessage] = useState('')
    const [showChat, setShowChat] = useState(false)
    const [chats, setChats] = useState([])
    const [chatOptionChosed, setChatOptionChosed] = useState('')

    useEffect(() => {
        if (props.role === "Admin")
        {
            fetchAdminMessages();
            setChats(chatOptionsNoSquad);
        }
        else
        {
            fetchMessages()
            if (!props.player) return;
            if (props.player.currentSquad === null){
                setChats(chatOptionsNoSquad)
            } else {
                setChats(chatOptionsInSquad)
            }
        }
    }, [])

    useEffect(() => {
        const interval = setInterval(() => {
            if (showChat === true && chatOptionChosed === 'Squad') {
                fetchSquadMessages();
            } else if(showChat === true){
                if (props.role === "Admin")
                    fetchAdminMessages();
                else fetchMessages();
            }
        }, 2000);
        return () => clearInterval(interval);
    },);

    const listChats = () => {
        if (props.role === "Admin") return;
        return chats.map((chat) => {
            return (
                <Button variant={chat.btn} onClick={() => selectChat(chat)}>{chat.name}</Button>
            )
        })
    }

    const selectChat = (chat) => {
        setChatOptionChosed(chat.name)
        let result
        if (props.player.currentSquad === null){
            result = chatOptionsNoSquad.map(option => {
                if (chat.name === option.name){
                    option.btn = 'selected'
                } else {
                    option.btn = 'group'
                }
                return option
            })
        } else {
            result = chatOptionsInSquad.map(option => {
                if (chat.name === option.name){
                    option.btn = 'selected'
                } else {
                    option.btn = 'group'
                }
                return option
            })
        }
        setChats(result)
    }

    const fetchMessages = async () => {
        try{
            const mess = await getChat(props.gameId, props.player.playerId);
            if (mess.length === messages.length) {
                return
            }
            setMessages(mess)
        }
        catch (error){
            console.error(error)
        }
    }

    const fetchAdminMessages = async () => {
        try{
            const mess = await getAdminChat(props.gameId);
            if (mess.length === messages.length) {
                return
            }
            setMessages(mess)
        }
        catch (error){
            console.error(error)
        }
    }

    const fetchSquadMessages = async () => {
        try{
            const mess = await getChatSquad(props.gameId, props.player.currentSquad, props.player.playerId)
            if (mess.length === messages.length) {
                return
            }
            setMessages(mess)
        }
        catch (error){
            console.error(error)
        }
    }

    // TODO: handle so that it takes in the player, and not just this mocked string for player
    const handleMessage = (msg) => {
        setMessage(msg)
        setMessages([...messages, {user: UserService.getUsername(), message: msg}])
    }

    const hideTheChat = () => {
        setShowChat(false);
    }
    const showTheChat = () => {
        setShowChat(true);
    }

    return (
        <div>
            {!showChat ?
                <div className="chat-btn-container">
                    <Button onClick={showTheChat} className="chat-btn" variant="customize">
                        <span className="material-icons">chat</span>
                    </Button>
                </div>
                :
                <div className="chat">
                    <div className="chat-header">
                        <Button onClick={hideTheChat} className="hide-btn" variant="customize">
                            <span className="material-icons">clear</span>
                        </Button>
                        <div className="chat-title">
                            <ButtonToolbar>
                                <ButtonGroup className="grouped-btns">
                                    {listChats()}
                                </ButtonGroup>
                            </ButtonToolbar>
                        </div>
                    </div>
                    <div className="chat-body">
                        <MessageList messages={messages}/>
                    </div>
                    <div className="chat-footer">
                        <MessageForm message={handleMessage} chatChosed = {chatOptionChosed}
                                     gameId = {props.gameId} player = {props.player}/>
                    </div>
                </div>}
        </div>
    )
}

export default Chat;
