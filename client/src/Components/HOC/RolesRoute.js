import PropTypes from 'prop-types'
import { Route } from "react-router-dom";
import UserService from "../../Services/UserService";
import Forbidden from "../ForbiddenPage/Forbidden";

const RolesRoute = ({ roles, children, ...rest }) => (
    <Route {...rest}>
        {UserService.hasRole(roles) ? children : <Forbidden/>}
    </Route>
)

RolesRoute.propTypes = {
    roles: PropTypes.arrayOf(PropTypes.string).isRequired,
}

export default RolesRoute
