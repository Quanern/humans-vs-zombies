import { Route, Redirect }  from 'react-router-dom';
import {AppRoutes} from "../../Constants/AppRoutes";

export const PrivateRoute = props => {
    if (false)
        return <Redirect to={AppRoutes.Login} exact/>

    return <Route {...props}/>
}

export default PrivateRoute;
