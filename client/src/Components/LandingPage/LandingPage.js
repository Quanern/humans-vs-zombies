import "./LandingPage.css"
import GameList from "../GameList/GameList";

function LandingPage()
{
    return (
        <div>
            <h1 className="listText">Game List</h1>
            <div className="gameList">
                <GameList/>
            </div>
        </div>
    );
}

export default LandingPage;
