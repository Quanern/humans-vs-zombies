import "./NavigationBar.css";

import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import RenderOnAnonymous from "../HOC/RenderOnAnonymous";
import RenderOnAuthenticated from "../HOC/RenderOnAuthenticated";
import UserService from "../../Services/UserService";
import RenderOnRole from "../HOC/RenderOnRole";
import {Link} from "react-router-dom";
import {AppRoutes} from "../../Constants/AppRoutes";

function NavigationBar() {
    return (
        <Navbar fixed="top" className="navbar" collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Navbar.Brand href={"/"}>
                <img style={{maxWidth: "100%", height: "auto"}}
                    src="/logo.png"
                    width="464"
                    height="37"
                    className="d-inline-block align-top"
                    alt="React Bootstrap logo"
                />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto">
                </Nav>
                <Nav>
                    <Form inline>
                        <RenderOnAnonymous>
                            <Button className="loginButton" variant="outline-warning" onClick={() => UserService.doLogin()}>
                                Sign In</Button>
                        </RenderOnAnonymous>
                        <RenderOnAuthenticated>
                            <h3 className="helloMsg">Hi {UserService.getFirstName()}</h3>
                            <RenderOnRole roles={['Admin']}>
                                <Link to={AppRoutes.AdminPage}><Button className="loginButton" variant="outline-warning">Administration</Button></Link>
                            </RenderOnRole>
                            <Button className="loginButton" variant="outline-warning" onClick={() => UserService.doLogout()}>Log Out</Button>
                        </RenderOnAuthenticated>
                    </Form>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}

export default NavigationBar;
