import "./Forbidden.css"

function Forbidden()
{
    return(
        <div>
            <h2 className="content">403: You are not authorized to access this content.</h2>
        </div>
    )
}

export default Forbidden;
