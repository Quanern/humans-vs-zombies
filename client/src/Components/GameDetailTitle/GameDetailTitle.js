import './GameDetailTitle.css'
import {useEffect, useState} from "react";
import {useLocation} from "react-router-dom";
import {getSpecificGame} from "../../Services/fetchMethods";

function GameDetailTitle(props) {
    const [game, setGame] = useState();

    const location = useLocation()
    //Gets the path of the current url, split into array with /
    const locationPath = location.pathname.split('/')
    //The last element in the locationpath, is the gameId
    const currentLocation = locationPath[locationPath.length - 1]

    useEffect(() => {
        const fetchGame = async () => {
            getSpecificGame(currentLocation).then(fetched => setGame(fetched))
        }
        fetchGame()
    },[])


    return (
        <div className="title">
            <div>
                <h1>{game && game.name}</h1>
                <h3><i>Status: {game && game.gameState.state}</i></h3>
            </div>
            <br/>
            <div>
                <h4>Description</h4>
                <p>{game && game.description ? game.description : "This game has no description"}</p>
                <h4>Rules</h4>
                <p>Survive until the time runs out, or kill all the humans.</p>
            </div>
        </div>
    )
}
export default GameDetailTitle;
