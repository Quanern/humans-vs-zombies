import "./BiteCodeEntry.css";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import {useState} from "react";
import {addKill} from "../../Services/fetchMethods";

function BiteCodeEntry(props)
{
    const [code, setCode] = useState("");
    const [location, setLocation] = useState(false);
    const [killDesc, setKillDesc] = useState("");

    const submitKill = () => {
        if (location){
            navigator.geolocation.getCurrentPosition( (position) => {
                const killObj = {
                    killer: props.player.playerId,
                    victim: code,
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                    story: killDesc
                }
                addKill(props.game.gameId, killObj).then((r) => {
                    alert("You successfully killed a human.");
                    window.location.reload(false);
                }).catch(() => {
                    alert("You've provided an incorrect bite code!");
                });
                },() => {
                const killObj = {
                    killer: props.player.playerId,
                    victim: code,
                    lat: 0,
                    lng: 0,
                    story: killDesc
                }
                addKill(props.game.gameId, killObj).then((r) => {
                    window.location.reload(false);
                }).catch(() => {
                    alert("You've provided an incorrect bite code!");
                });
                alert("Could not include kill location because access to your current location is blocked or disabled.")
            });
        }
        else
        {
            const killObj = {
                killer: props.player.playerId,
                victim: code,
                lat: 0,
                lng: 0,
                story: killDesc
            }
            addKill(props.game.gameId, killObj).then((r) => {
                alert("You successfully killed a human.");
                window.location.reload(false);
            }).catch(() => {
                alert("You've provided an incorrect bite code!");
            });
        }
    }

    const handleCode = (e) => {
        setCode(e.target.value);
    }

    const handleLocation = (e) => {
        setLocation(e.target.checked)
    }

    const handleKillDesc = (e) => {
        setKillDesc(e.target.value);
    }

    return (
            <Form className="bite-code-entry">
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Bite Code</Form.Label>
                    <Form.Control type="text" placeholder="Enter bite code..." onChange={handleCode}/>
                </Form.Group>
                <Form.Group controlId="formBasicCheckbox">
                    <div className="gps-input">
                        <Form.Check type="checkbox" label="Include my location in the kill" onChange={handleLocation}/>
                    </div>
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Kill Description</Form.Label>
                    <Form.Control placeholder="Enter victim's death..." as="textarea" rows={3} onChange={handleKillDesc}/>
                </Form.Group>
                <Button variant="primary" onClick={submitKill}>Kill</Button>
            </Form>
    )
}

export default BiteCodeEntry;
