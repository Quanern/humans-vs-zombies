import './BiteCodeEntry.css'
function BiteCode (props)
{
    return (
        <div>
            <h4 style={{color: "white"}} className="bite-code-title">Your Bite Code: {props.biteCode}</h4>
        </div>
    )
}

export default BiteCode;
