import 'leaflet/dist/leaflet.css';
import { MapContainer, TileLayer } from 'react-leaflet'
import MapRectangle from "./MapRectangle";
import MapMarker, {Markers} from "./MapMarker";
import {useEffect, useState} from "react";
import {getAdminCheckIn, getCheckIn, getKillsInGame, getMissions} from "../../Services/fetchMethods";

const containerStyle = {
    width: (window.innerWidth < 1000) ? window.innerWidth + 'px' : window.innerWidth * 0.56 + 'px',
    height: (window.innerHeight * 0.56) + 'px'
};

function Map(props) {
    const [deaths, setDeaths] = useState([]);
    const [missions, setMissions] = useState([]);
    const [squadPings, setSquadPings] = useState([]);

    useEffect(() => {
        const getKills = async () => {
            if (!props.game) return;
            const kills = await getKillsInGame(props.game.gameId);
            if (!kills) return [];
            setDeaths(kills);
        }

        const getGameMissions = async () => {
            if (!props.game) return;
            const missionMarkers = await getMissions(props.game.gameId);
            if (!missionMarkers) return [];
            setMissions(missionMarkers);
        }

        const getSquadCheckIns = async () => {
            if (props.role === "Admin")
            {
                if (!props.game) return;
                const pings = await getAdminCheckIn(props.game.gameId);
                if (!pings) return;
                setSquadPings(pings);
            }
            else
            {
                if (!props.player || !props.game || !props.player.currentSquad) return;
                const pings = await getCheckIn(props.game.gameId, props.player.currentSquad, props.player.playerId);
                if (!pings) return;
                setSquadPings(pings);
            }
        }

        getSquadCheckIns();
        getKills();
        getGameMissions();
    }, [props.game, props.player, props.role]);

    const renderMissions = () => {
        if (!missions) return;
        return missions.filter((mission) => {
            if (props.role === "Admin")
                return true;
            else if (props.role === "Player" || props.role === "Squad Member")
            {
                console.log("here")
                if ((mission.humanVisible && mission.zombieVisible))
                    return true;
                else if ((mission.humanVisible && !mission.zombieVisible) && props.player.human)
                    return true;
                else if ((!mission.humanVisible && mission.zombieVisible) && !props.player.human)
                    return true;
                else return false;
            } else return false;
            }).map((mission, index) => {
            return(
                <MapMarker key={index} type={Markers.mission} text={mission.name} latitude={Number(mission.lat)} longitude={Number(mission.lng)}/>
            )
        })
    }

    const renderDeaths = () => {
        if (!deaths) return;
        return deaths.map((death, index) => {
            return(
                <MapMarker key={index} type={Markers.gravestone} text={death.story} latitude={Number(death.lat)} longitude={Number(death.lng)}/>
            )
        })
    }

    const renderSquadsPings = () => {
        if (!squadPings) return;
        return squadPings.filter((ping) => { return props.role === "Admin" || props.role === "Squad Member"; }).map((ping, index) => {
            return(
                <MapMarker key={index} type={Markers.checkIn} text={"Squad Member " + ping.squadMember} latitude={Number(ping.lat)} longitude={Number(ping.lng)}/>
            )
        })
    }

    return (
        <div>
            <MapContainer style={{ height: containerStyle.height, width: containerStyle.width }} center={[ (props.latSouth + props.latNorth) / 2, (props.lngWest + props.lngEast) / 2]} zoom={7}>
                <TileLayer url="https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png"/>
                <MapRectangle latSouth={props.latSouth} longWest={props.lngWest} latNorth={props.latNorth} longEast={props.lngEast}/>
                {renderSquadsPings()}
                {renderDeaths()}
                {renderMissions()}
            </MapContainer>
        </div>
    )
}

export default Map;
