import "leaflet/dist/leaflet.css";
import {Marker, Popup} from "react-leaflet";
import L from "leaflet";

export const Markers = {
    gravestone: "gravestone",
    checkIn: "squad check in",
    mission: "mission"
};

function MapMarker(props)
{
    const getMarkerIcon = () => {
        if (props.type === "gravestone")
            return "https://iconsplace.com/wp-content/uploads/_icons/ffffff/256/png/headstone-icon-18-256.png";
        else if (props.type === "squad check in")
            return "https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/White_check.svg/600px-White_check.svg.png";
        else if (props.type === "mission")
            return "https://www.pngkey.com/png/full/430-4308298_our-mission-vision-mission-icon-white.png";
    }
    const gravestone = L.icon({
        iconUrl: getMarkerIcon(),
        iconAnchor: [15,15],
        iconSize: [30,30]
    });
    return(
        <div>
            <Marker icon={gravestone} position={[props.latitude, props.longitude]}>
                <Popup>
                    {props.text}
                </Popup>
            </Marker>
        </div>
    );
}

export default MapMarker;
