import {Rectangle, useMap} from "react-leaflet";
import {useState, useMemo} from "react";

function MapRectangle(props) {
    const innerBounds = [
        [props.latSouth, props.longWest],
        [props.latNorth, props.longEast]
    ]
    const [bounds, setBounds] = useState(innerBounds)
    const map = useMap()

    const innerHandlers = useMemo(
        () => ({
            click() {
                setBounds(innerBounds)
                map.fitBounds(innerBounds)
            },
        }),
        [map],
    )

    return (
        <div>
            <Rectangle
                bounds={innerBounds}
                eventHandlers={innerHandlers}
                pathOptions={{ color: "red", fillColor: "transparent"}}
            />
        </div>
    )
}

export default MapRectangle;
