import "./NotFound.css"

function NotFound()
{
    return (
      <div>
          <h2 className="content">404: Page not found</h2>
      </div>
    );
}

export default NotFound;
