import "./GameListCard.css";
import Card from "react-bootstrap/Card";
import Accordion from "react-bootstrap/Accordion";
import Button from "react-bootstrap/Button";
import {Link} from "react-router-dom";
import RenderOnAuthenticated from "../HOC/RenderOnAuthenticated";

function GameListCard(props)
{
    return (
        <Card bsPrefix="games-card">
            <Accordion.Toggle as={Card.Header} eventKey={props.eventKey}>
                {props.name}
            </Accordion.Toggle>
            <Accordion.Collapse eventKey={props.eventKey}>
                <Card.Body className="games-card-body">
                    <div className="games-div">
                        <b>State: {props.state}</b>
                        <b>Since: {new Date(props.date).toLocaleString()}</b>
                        <b>Players: {props.players}</b>
                    </div>
                    <div className="games-button-div">
                        <RenderOnAuthenticated>
                            <Link to={"game/" + props.gameId}><Button variant="primary">Details</Button></Link>
                        </RenderOnAuthenticated>
                    </div>
                </Card.Body>
            </Accordion.Collapse>
        </Card>
    );
}

export default GameListCard;
