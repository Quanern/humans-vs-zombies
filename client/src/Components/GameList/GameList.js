import Accordion from "react-bootstrap/Accordion";
import GameListCard from "./GameListCard";
import {useEffect, useState} from "react";
import {getGames} from "../../Services/fetchMethods";

function GameList()
{
    const [game, setGames] = useState([]);
    useEffect(() => {
        const func = async () => {
            const games = await getGames().catch(err => {return []});
            setGames(games);
        }
        func();
    }, []);

    return(
        <div>
            <Accordion>
                {
                    game.sort((a,b) => a.gameId - b.gameId).map((item, index) => (
                        <GameListCard date={item.start} state={item.gameState.state} players={item.players.length} key={index} gameId={item.gameId} name={item.name} eventKey={index+1}/>
                    ))
                }
            </Accordion>
        </div>
    );
}

export default GameList;
