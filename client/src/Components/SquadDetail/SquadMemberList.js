import {useEffect, useState} from "react";
import {Button, Table} from "react-bootstrap";
import {getSpecificSquad, squadCheckin} from "../../Services/fetchMethods";

function SquadMemberList(props) {

    const [members, setMembers] = useState([])

    useEffect(() => {
        const fetchMembers = async() => {
            try {
                const squad = await getSpecificSquad(props.gameId, props.squadId)
                console.log(squad.squadMembers)
                setMembers(squad.squadMembers)
            } catch (e) {
                console.log(e.message)
            }
        }
        fetchMembers()
    }, [])

    const displayMembers = () => {
        return members.map((member, index) => {
            return (
                <tr key={index} className="table-elements">
                    <td>{member.username}</td>
                    <td>{member.rank}</td>
                    <td>{member.killed ? 'Killed' : 'Alive'}</td>
                </tr>
            )
        })
    }
    return (
        <div className="table-detail-wrapper">
            <Table className="detail-table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Rank</th>
                    <th>State</th>
                </tr>
                </thead>
                <tbody>
                {displayMembers()}
                </tbody>
            </Table>
        </div>
    )
}
export default SquadMemberList;
