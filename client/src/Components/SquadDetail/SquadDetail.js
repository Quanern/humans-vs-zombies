import './SquadDetail.css'
import SquadMemberList from "./SquadMemberList";
import {Button} from "react-bootstrap";
import {getSpecificSquad, leaveSquad, squadCheckin} from "../../Services/fetchMethods";
import {useEffect, useState} from "react";

function SquadDetail(props) {
    const gameId = props.game.gameId;
    const squadId = props.player.currentSquad;
    const [squad, setSquad] = useState();

    useEffect(() => {
       const squadFunc = async() => {
           const s = await getSpecificSquad(gameId, squadId);
           setSquad(s);
       }
       squadFunc();
    }, [squadId]);

    const checkMarker = () => {
        navigator.geolocation.getCurrentPosition( (position) => {
                const checkInObject = {squadMember: props.player.playerId, lat: position.coords.latitude, lng: position.coords.longitude} //TODO: Change the hard coded values
                squadCheckin(gameId, squadId, checkInObject).then((r) => {
                    alert(`You checked a marker at latitude: ${checkInObject.lat}, longitude: ${checkInObject.lng}`)
                })
            },
            function(error) {
                alert("Could not check-in because access to your current location is blocked or disabled.");
            }
        );
    }

    const leaveTheSquad = () => {
        const playerLeaveSquad = {playerId: props.player.playerId}
        leaveSquad(gameId, squadId, playerLeaveSquad).then(() => {
            window.location.reload(false);
        })

    }

    return (
        <div className="detail-wrapper">
            <div className="detail-elements">
                <h4 className="info-header">Squad {squad && squad.name}</h4>
                <Button variant="danger" className="btn-leave-sq" onClick={leaveTheSquad}>Leave squad</Button>
                <div className="btn-inf-head">
                    <Button variant="info" onClick={checkMarker}>Check marker</Button>
                </div>
                <SquadMemberList gameId = {gameId} squadId = {squadId}/>
            </div>
        </div>
    )
}
export default SquadDetail;
