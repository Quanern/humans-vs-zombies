import "./Login.css"
import RenderOnAuthenticated from "../HOC/RenderOnAuthenticated";
import {useEffect} from "react";
import RenderOnAnonymous from "../HOC/RenderOnAnonymous";
import UserService from "../../Services/UserService";
import {useHistory} from "react-router-dom"
import {AppRoutes} from "../../Constants/AppRoutes";
import {postInit} from "../../Services/fetchMethods";
import {URI} from "../../Constants/uri";

function Login() {
    let history = useHistory()

    useEffect(() => {
        if (UserService.isLoggedIn()) {
            UserService.updateToken(() => {
                fetch(` ${URI.ResourceServer.Default}/api/v1/user`, postInit())
                    .then(res => console.log(res))
                    .catch(err => console.log(err))

            }).catch(err => console.log(err))
        }
        const timer = setTimeout(() =>{
            history.push(AppRoutes.LandingPage);
        }, 2000)

        return () => clearTimeout(timer)
    }, [])
    return(
        <div>
            <RenderOnAuthenticated>
                <h2 className="content">Login Success, redirecting you to landing page!</h2>
            </RenderOnAuthenticated>
            <RenderOnAnonymous>
                <h2 className="content">Login failed, redirecting you to landing page!</h2>
            </RenderOnAnonymous>
        </div>


    )

}
export default Login;
