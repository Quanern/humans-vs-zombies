import './RegistrationFragment.css'
import {Button} from "react-bootstrap";
import {useEffect, useState} from "react";
import {addPlayer} from "../../Services/fetchMethods";

function RegistrationFragment(props){
    const [clicked, setClicked] = useState();
    useEffect(() => {
        setClicked(props.hasRegistered ? true : false);
    }, [props.hasRegistered])
    const btnClicked = () => {
        if (!clicked){
            addPlayer(props.gameId, { user: String(props.username) }).then(() => {
                window.location.reload(false);
                setClicked(true);
            });
        } else {
            setClicked(false)
        }
    }

    return (
        <div className="reg-btn">
            <Button variant="success" onClick={btnClicked} disabled={clicked}>
                {!clicked ? 'Join game' : 'Joined'}
            </Button>
        </div>
    )
}
export default RegistrationFragment;
