import {useEffect, useState} from "react";
import {ListGroup} from "react-bootstrap";
import CreateGame from "./CreateGame";
import Form from "react-bootstrap/Form";
import EditGame from "./EditGame";

function Games(props) {
    const [games, setGames] = useState(props.games);
    const [filtered, setFiltered] = useState(props.games);

    const filterChange = (e) => {
        if (e.target.value === "") setFiltered(games);
        else setFiltered(games.filter(item => {
            if (item.name) return item.name.toLowerCase().trim().includes(e.target.value.toLowerCase().trim())
            else return false;
        }));
    }

    useEffect(() => {
        setGames(props.games);
        setFiltered(props.games);
    }, [props.games]);

    const listItems = () => {
        return filtered.sort((a,b) => a.gameId - b.gameId).map((game, index) => {
            return (
                <ListGroup.Item className="list-item" action key={index}>
                    {game.name}
                    <EditGame gameEdited={props.gamesEdited} game={game}>Edit Game</EditGame>
                </ListGroup.Item>
            )
        })
    }

    return(
        <div>
            <div className="game-header">
                <h3>Games</h3>
                <div className="header-content">
                    <CreateGame gameEdited={props.gamesEdited} className="btn"/>
                    <Form.Control className="header-search" type="text" placeholder="Search games..." onChange={filterChange}/>
                </div>
            </div>
            <div>
                <ListGroup className="list-display-game">
                    {listItems()}
                </ListGroup>
            </div>
        </div>
    )
}
export default Games;
