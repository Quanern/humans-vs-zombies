import Button from "react-bootstrap/Button";
import {Modal} from "react-bootstrap";
import Form from "react-bootstrap/Form";
import {useState} from "react";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import {deleteMission, updateMission} from "../../Services/fetchMethods";

function EditMission(props)
{
    const [showMain, setShowMain] = useState(false)
    const [showDelete, setShowDelete] = useState(false)
    const [mission, setMission] = useState(props.mission);
    const [stateTitle, setStateTitle] = useState();

    const handleCloseMain = () => { setShowMain(false); handleCloseDelete();}
    const handleShowMain = () => setShowMain(true)

    const handleCloseDelete = () => setShowDelete(false)
    const handleShowDelete = () => setShowDelete(true)

    const handleChange = (e) =>{
        setMission({...mission, [e.target.name] : e.target.value});
    }

    const getState = () => {
        let state;
        const m = props.mission;
        if (m.humanVisible && m.zombieVisible) state = "Global";
        else if (m.humanVisible && !m.zombieVisible) state = "Only Humans";
        else if (!m.humanVisible && m.zombieVisible) state = "Only Zombies";
        else state = "Invalid";
        return state;
    }

    useState(() => {
        setStateTitle(getState());
    },[]);

    //TODO: Handle if the inputs are valid
    const handleFormSubmit = (e) => {
        e.preventDefault();
        updateMission(mission).then(() => {
            setShowMain(false);
            setShowDelete(false);
            props.missionEdited();
        });

    }

    const removeMission = () => {
        deleteMission(mission.game, mission.missionId).then(() => {
            props.missionEdited();
            setShowDelete(false);
            setShowMain(false);
        });
    }

    const stateChanged = (e, object) => {
        setStateTitle(object.target.outerText);
        if (e === "1") setMission({...mission, humanVisible: true, zombieVisible: true});
        else if (e === "2") setMission({...mission, humanVisible: true, zombieVisible: false});
        else if (e === "3") setMission({...mission, humanVisible: false, zombieVisible: true});
    }

    return(
        <div>
            <Button onClick={handleShowMain} variant="warning" className="list-btn" size="sm">
                Edit Mission
            </Button>
            <Modal show={showMain} onHide={handleCloseMain}>
                <Modal.Header closeButton className="reg-modal-header">
                    <Modal.Title>Edit {props.mission.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body className="reg-modal-body">
                    <Form>
                        <Form.Group>
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter game name..." name='name' value={mission.name} onChange={handleChange}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" placeholder={"Enter description..."} name='description' value={mission.description} onChange={handleChange}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Coordinates</Form.Label>
                            <div className="game-form-inputs">
                                <Form.Control type="text" placeholder={"Enter latitude..."} className="game-form-input" name='lat' value={mission.lat} onChange={handleChange}/>
                                <Form.Control type="text" placeholder={"Enter longitude..."} className="game-form-input" name='lng' value={mission.lng} onChange={handleChange}/>
                            </div>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Dates</Form.Label>
                            <div className="game-form-inputs">
                                <Form.Control type="text" placeholder={mission.startTime ? new Date(mission.startTime).toLocaleDateString() : "Enter start date..."} className="game-form-input" name='startTime' value={mission.startTime} onChange={handleChange}/>
                                —<Form.Control type="text" placeholder={mission.endTime ? new Date(mission.endTime).toLocaleDateString() :"Enter start date..."} className="game-form-input" name='endTime'value={mission.endTime} onChange={handleChange}/>
                            </div>
                        </Form.Group>
                        <Form.Label>Map Visibility</Form.Label>
                        <DropdownButton variant="info" id="dropdown-basic-button" title={stateTitle} onSelect={stateChanged}>
                            <Dropdown.Item eventKey={1}>Global</Dropdown.Item>
                            <Dropdown.Item eventKey={2}>Only Humans</Dropdown.Item>
                            <Dropdown.Item eventKey={3}>Only Zombies</Dropdown.Item>
                        </DropdownButton>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" onClick={handleShowDelete} className="delete-btn">
                        Delete
                    </Button>
                    <Button variant="warning" onClick={handleCloseMain}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleFormSubmit}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
            <Modal show={showDelete} onHide={handleCloseDelete}>
                <Modal.Header closeButton className="reg-modal-header">
                    <Modal.Title>Delete Game</Modal.Title>
                </Modal.Header>
                <Modal.Body className="reg-modal-body">
                    Are you sure you want to delete this mission?
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="warning" onClick={handleCloseDelete}>
                        Cancel
                    </Button>
                    <Button variant="primary" onClick={removeMission}>
                        Confirm
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default EditMission;
