import {useEffect, useState} from "react";
import {Button, ListGroup} from "react-bootstrap";
import {getPlayers} from "../../Services/fetchMethods";
import SelectGame from "./SelectGame";
import Form from "react-bootstrap/Form";
import EditPlayer from "./EditPlayer";

function Players(props) {
    const [players, setPlayers] = useState([]);
    const [filtered, setFiltered] = useState([]);
    const [gameId, setGameId] = useState((props.games.length) ? props.games[0].gameId : 1);

    const gameIdChanged = id => {
        setGameId(id);
    }

    const filterChange = (e) => {
        if (e.target.value === "") setFiltered(players);
        else setFiltered(players.filter(item => {
            if (item.user.username) return item.user.username.toLowerCase().trim().includes(e.target.value.toLowerCase().trim());
            else return false;
        }));
    }
    const fetchPlayers = async () => {
        try {
            const pl = await getPlayers(gameId);
            setPlayers(new Array(...pl));
            setFiltered(new Array(...pl));
        }
        catch (e) {
            setPlayers([]);
            setFiltered([]);
        }
    }

    useEffect(() => {
        fetchPlayers()
    },[gameId])

    const deletePlayer = (id) => {
        setPlayers(players.filter(player => player.playerId !== id));
        setFiltered(players.filter(player => player.playerId !== id));
        console.log(players);
    }

    const editPlayer = () => {
        fetchPlayers();
    }

    const listItems = () => {
        if (!filtered) return;
        return filtered.map((player, index) => {
            return (
                <ListGroup.Item className="list-item" action key={index}>
                    {player.user.username}
                    <EditPlayer playerEdited={editPlayer} player={player}/>
                </ListGroup.Item>
            )
        })
    }

    return(
        <div>
            <div>
                <h3>Players</h3>
                <div className="header-content">
                    <SelectGame games={props.games} gameChanged={gameIdChanged}/>
                    <Form.Control className="header-search" type="text" placeholder="Search players..." onChange={filterChange}/>
                </div>
            </div>
            <div className="pl-list">
                <ListGroup className="list-display">
                    {listItems()}
                </ListGroup>
            </div>
        </div>
    )
}
export default Players
