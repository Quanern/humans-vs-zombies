import {useState} from "react";
import Button from "react-bootstrap/Button";
import {Modal} from "react-bootstrap";
import Form from "react-bootstrap/Form";
import {addGame} from "../../Services/fetchMethods";

function CreateGame(props) {
    const [show, setShow] = useState(false)
    const [game, setGame] = useState({name: '', selng: '', selat: '', nwlat: '', nwlng: ''})

    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)

    const handleChange = (e) => {
        setGame({ ...game, [e.target.name] : e.target.value })
    }

    //TODO: Handle if the inputs are valid
    const handleFormSubmit = (e) => {
        e.preventDefault()
        addGame(game).then(() =>{
                props.gameEdited();
                setShow(false)
            }
        )
    }

    return (
        <>
            <Button variant="success" onClick={handleShow} size="sm" className="btn-admin-page">
                Create
            </Button>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton className="reg-modal-header">
                    <Modal.Title>Create game</Modal.Title>
                </Modal.Header>
                <Modal.Body className="reg-modal-body">
                    <Form>
                        <Form.Group>
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" placeholder="Write game name..." name='name' value={game.name} onChange={handleChange}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" placeholder={"Enter description..."} name='description' value={game.description} onChange={handleChange}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Game area</Form.Label>
                            <div className="game-form-inputs">
                                <Form.Control type="text" placeholder={"Enter latitude south..."} className="game-form-input" name='south' value={game.south} onChange={handleChange}/>
                                <Form.Control type="text" placeholder={"Enter longitude west..."} className="game-form-input" name='west' value={game.west} onChange={handleChange}/>
                                <Form.Control type="text" placeholder={"Enter latitude north..."} className="game-form-input" name='north' value={game.north} onChange={handleChange}/>
                                <Form.Control type="text" placeholder={"Enter longitude east..."} className="game-form-input" name='east' value={game.east} onChange={handleChange}/>
                            </div>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="warning" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleFormSubmit}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
export default CreateGame;
