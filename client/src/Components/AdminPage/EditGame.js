import Button from "react-bootstrap/Button";
import {Modal} from "react-bootstrap";
import Form from "react-bootstrap/Form";
import {useState} from "react";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import {updateGame, deleteGame} from "../../Services/fetchMethods";

function EditGame(props)
{
    const [showMain, setShowMain] = useState(false)
    const [showDelete, setShowDelete] = useState(false)
    const [game, setGame] = useState(
        {
            gameId: props.game.gameId,
            gameState: { state: props.game.gameState.state, stateId: props.game.gameState.stateId },
            name: props.game.name,
            description: props.game.description,
            south: props.game.south,
            west: props.game.west,
            east: props.game.east,
            north: props.game.north
        });
    const [stateTitle, setStateTitle] = useState(props.game.gameState.state);

    const handleCloseMain = () => { setShowMain(false); handleCloseDelete();}
    const handleShowMain = () => setShowMain(true)

    const handleCloseDelete = () => setShowDelete(false)
    const handleShowDelete = () => setShowDelete(true)

    const handleChange = (e) =>{
        setGame({...game, [e.target.name] : e.target.value});
    }

    //TODO: Handle if the inputs are valid
    const handleFormSubmit = (e) => {
        e.preventDefault();
        updateGame(game).then(() => {
                setShowMain(false);
                setShowDelete(false);
                props.gameEdited();
            }
        );
    }

    const removeGame = () => {
        deleteGame(game.gameId).then(() => {
            setShowMain(false);
            setShowDelete(false);
            props.gameEdited();
        });
    }
    const stateChanged = (e, object) =>{
        setStateTitle(object.target.outerText);
        setGame({...game, gameState: {  state: object.target.outerText, stateId: Number(e) } });
    }

    return(
      <div>
          <Button onClick={handleShowMain} variant="warning" className="list-btn" size="sm">
              Edit Game
          </Button>
          <Modal show={showMain} onHide={handleCloseMain}>
              <Modal.Header closeButton className="reg-modal-header">
                  <Modal.Title>Edit {props.game.name}</Modal.Title>
              </Modal.Header>
              <Modal.Body className="reg-modal-body">
                  <Form>
                      <Form.Group>
                          <Form.Label>Name</Form.Label>
                          <Form.Control type="text" placeholder="Enter game name..." name='name' value={game.name} onChange={handleChange}/>
                      </Form.Group>
                      <DropdownButton variant="info" id="dropdown-basic-button" title={stateTitle} onSelect={stateChanged}>
                          <Dropdown.Item eventKey={1}>Registration</Dropdown.Item>
                          <Dropdown.Item eventKey={2}>In Progress</Dropdown.Item>
                          <Dropdown.Item eventKey={3}>Completed</Dropdown.Item>
                      </DropdownButton>
                      <br/>
                      <Form.Group>
                          <Form.Label>Description</Form.Label>
                          <Form.Control type="text" placeholder={"Enter description..."} name='description' value={game.description} onChange={handleChange}/>
                      </Form.Group>
                      <Form.Group>
                          <Form.Label>Game area</Form.Label>
                          <div className="game-form-inputs">
                              <Form.Control type="text" placeholder={"Enter latitude south..."} className="game-form-input" name='south' value={game.south} onChange={handleChange}/>
                              <Form.Control type="text" placeholder={"Enter longitude west..."} className="game-form-input" name='west' value={game.west} onChange={handleChange}/>
                              <Form.Control type="text" placeholder={"Enter latitude north..."} className="game-form-input" name='north' value={game.north} onChange={handleChange}/>
                              <Form.Control type="text" placeholder={"Enter longitude east..."} className="game-form-input" name='east' value={game.east} onChange={handleChange}/>
                          </div>
                      </Form.Group>
                  </Form>
              </Modal.Body>
              <Modal.Footer>
                  <Button variant="danger" onClick={handleShowDelete} className="delete-btn">
                      Delete
                  </Button>
                  <Button variant="warning" onClick={handleCloseMain}>
                      Close
                  </Button>
                  <Button variant="primary" onClick={handleFormSubmit}>
                      Save Changes
                  </Button>
              </Modal.Footer>
          </Modal>
          <Modal show={showDelete} onHide={handleCloseDelete}>
              <Modal.Header closeButton className="reg-modal-header">
                  <Modal.Title>Delete Game</Modal.Title>
              </Modal.Header>
              <Modal.Body className="reg-modal-body">
                Are you sure you want to delete this game?
              </Modal.Body>
              <Modal.Footer>
                  <Button variant="warning" onClick={handleCloseDelete}>
                      Cancel
                  </Button>
                  <Button variant="primary" onClick={removeGame}>
                      Confirm
                  </Button>
              </Modal.Footer>
          </Modal>
      </div>
    );
}

export default EditGame;
