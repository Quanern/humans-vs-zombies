import Games from "./Games";
import Missions from "./Missions";
import Players from "./Players";
import './AdminPage.css'
import {getGames} from "../../Services/fetchMethods";
import {useEffect, useState} from "react";

function AdminPage () {

    const [gamesArray, setGames] = useState([]);

    const fetchGames = async () => {
        try {
            const fetched = await getGames();
            setGames(new Array(...fetched));
        }
        catch (e) {
            setGames([]);
        }
    }

    useEffect(() => {
        fetchGames();
    }, []);

    const refreshGames = () => {
        fetchGames();
    }

    return (
        <div>
            <h1 className="text">Administrator view</h1>
            <div  className="admin-wrapper">
                <div className="games">
                    <Games gamesEdited={refreshGames} games={gamesArray}/>
                </div>
                <div className="players">
                    <Players games={gamesArray}/>
                </div>
                <div className="missions">
                    <Missions games={gamesArray}/>
                </div>
            </div>
        </div>
    )
}

export default AdminPage;
