import Button from "react-bootstrap/Button";
import {Modal} from "react-bootstrap";
import Form from "react-bootstrap/Form";
import {useState} from "react";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import {updatePlayer, deletePlayer} from "../../Services/fetchMethods";

function EditGame(props)
{
    const [showMain, setShowMain] = useState(false)
    const [showDelete, setShowDelete] = useState(false)
    const [player, setPlayer] = useState(props.player);
    const [stateTitle, setStateTitle] = useState("Change State");

    const handleCloseMain = () => { setShowMain(false); handleCloseDelete();}
    const handleShowMain = () => setShowMain(true)

    const handleCloseDelete = () => setShowDelete(false)
    const handleShowDelete = () => setShowDelete(true)

    //TODO: Handle if the inputs are valid
    const handleFormSubmit = (e) => {
        e.preventDefault();
        updatePlayer(props.player.game, props.player.playerId, player).then(() => {
                props.playerEdited();
                setShowMain(false);
                setShowDelete(false);
            }
        );
    }

    const removePlayer = () => {
        deletePlayer(props.player.game, props.player.playerId).then(() => {
            setShowDelete(false);
            setShowMain(false);
            props.playerEdited();
        });
    }
    const stateChanged = (e, object) =>{
        setStateTitle(object.target.outerText);
        setPlayer({human: e === "Human"});
    }

    return(
        <div>
            <Button onClick={handleShowMain} variant="warning" className="list-btn" size="sm">
                Edit Player
            </Button>
            <Modal show={showMain} onHide={handleCloseMain}>
                <Modal.Header closeButton className="reg-modal-header">
                    <Modal.Title>Edit {props.player.user.username}</Modal.Title>
                </Modal.Header>
                <Modal.Body className="reg-modal-body">
                    <Form>
                        <p>Joined Game ID: {props.player.game}</p>
                        <p>Player ID: {props.player.playerId}</p>
                        <p>Bite Code: {props.player.biteCode}</p>
                        <p>Status: {props.player.human ? "Alive" : "Deceased"}</p>
                        <DropdownButton variant="info" id="dropdown-basic-button" title={stateTitle} onSelect={stateChanged}>
                            <Dropdown.Item eventKey={"Human"}>Human</Dropdown.Item>
                            <Dropdown.Item eventKey={"Zombie"}>Zombie</Dropdown.Item>
                        </DropdownButton>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" onClick={handleShowDelete} className="delete-btn">
                        Delete
                    </Button>
                    <Button variant="warning" onClick={handleCloseMain}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleFormSubmit}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
            <Modal show={showDelete} onHide={handleCloseDelete}>
                <Modal.Header closeButton className="reg-modal-header">
                    <Modal.Title>Delete Player</Modal.Title>
                </Modal.Header>
                <Modal.Body className="reg-modal-body">
                    Are you sure you want to delete this player from their associated game?
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="warning" onClick={handleCloseDelete}>
                        Cancel
                    </Button>
                    <Button variant="primary" onClick={removePlayer}>
                        Confirm
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default EditGame;
