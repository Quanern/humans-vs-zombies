import "./AdminPage.css";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";

import {useEffect, useState} from "react";

function SelectGame(props)
{
    const [title, setTitle] = useState("No games found");
    const [games, setGames] = useState([]);

    const gameChanged = (e, obj) => {
        setTitle(obj.target.outerText);
        props.gameChanged(e);
    }

    useEffect(() => {
        if (props.games.length === 0) return;
        setGames(props.games);
        setTitle(props.games ? props.games[0].name : "No games found");
    }, [props.games]);

    const listGames = () => {
        return games.map((game, index) => {
            return(
                <Dropdown.Item key={index} eventKey={game.gameId}>{game.name}</Dropdown.Item>
            );
        });
    }

    return(
        <DropdownButton variant="info" id="dropdown-basic-button" title={title} onSelect={gameChanged}>
            {listGames()}
        </DropdownButton>
    )
}

export default SelectGame;
