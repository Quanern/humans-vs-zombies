import {useState} from "react";
import Button from "react-bootstrap/Button";
import {Modal} from "react-bootstrap";
import Form from "react-bootstrap/Form";
import {addMission} from "../../Services/fetchMethods";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";

function CreateMission(props) {
    const [show, setShow] = useState(false)
    const [mission, setMission] = useState({name: '', humanVisible: true, zombieVisible: true, description: '', startTime: '', endTime: '', game: 1})
    const [stateTitle, setStateTitle] = useState("Global");

    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)

    const handleChange = (e) => {
        setMission({ ...mission, [e.target.name] : e.target.value })
    }
    const handleSubmit = (e) => {
        e.preventDefault()
        addMission(props.gameId, mission).then(() =>{
                props.missionsEdited();
                setShow(false);
        })
    }

    const stateChanged = (e, object) => {
        setStateTitle(object.target.outerText);
        if (e === "1") setMission({...mission, humanVisible: true, zombieVisible: true});
        else if (e === "2") setMission({...mission, humanVisible: true, zombieVisible: false});
        else if (e === "3") setMission({...mission, humanVisible: false, zombieVisible: true});
    }

    return (
        <>
            <Button variant="success" onClick={handleShow} size="sm" className="btn-admin-page">
                Create
            </Button>
            <Modal show={show} onHide={handleClose} className="reg-modal">
                <Modal.Header closeButton className="reg-modal-header">
                    <Modal.Title>Create mission</Modal.Title>
                </Modal.Header>
                <Modal.Body className="reg-modal-body">
                    <Form>
                        <Form.Group>
                            <Form.Label>Title</Form.Label>
                            <Form.Control type="text" placeholder="Write mission title..." name='name' value={mission.name} onChange={handleChange}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Mission description</Form.Label>
                            <Form.Control type="text" placeholder="Enter mission description..." name='description' value={mission.description} onChange={handleChange}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Coordinates</Form.Label>
                            <div className="game-form-inputs">
                                <Form.Control type="text" placeholder={"Enter latitude..."} className="game-form-input" name='lat' value={mission.lat} onChange={handleChange}/>
                                <Form.Control type="text" placeholder={"Enter longitude..."} className="game-form-input" name='lng' value={mission.lng} onChange={handleChange}/>
                            </div>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Start time</Form.Label>
                            <Form.Control type="text" placeholder="Format: YYYY-MM-DD" name='startTime' value={mission.startTime} onChange={handleChange}/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>End time</Form.Label>
                            <Form.Control type="text" placeholder="Format: YYYY-MM-DD" name='endTime' value={mission.endTime} onChange={handleChange}/>
                        </Form.Group>
                        <Form.Label>Map Visibility</Form.Label>
                        <DropdownButton variant="success" id="dropdown-basic-button" title={stateTitle} onSelect={stateChanged}>
                            <Dropdown.Item eventKey={1}>Global</Dropdown.Item>
                            <Dropdown.Item eventKey={2}>Only Humans</Dropdown.Item>
                            <Dropdown.Item eventKey={3}>Only Zombies</Dropdown.Item>
                        </DropdownButton>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="warning" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleSubmit}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
export default CreateMission;
