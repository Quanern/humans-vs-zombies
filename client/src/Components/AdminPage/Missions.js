import {useEffect, useState} from "react";
import {Button, ListGroup} from "react-bootstrap";
import CreateMission from "./CreateMission";

import Form from "react-bootstrap/Form";
import {getMissions} from "../../Services/fetchMethods";
import SelectGame from "./SelectGame";
import EditMission from "./EditMission";

function Missions(props){
    const [missions, setMissions] = useState([]);
    const [filtered, setFiltered] = useState(new Array(...missions))
    const [gameId, setGameId] = useState((props.games.length) ? props.games[0].gameId : 1);

    const filterChange = (e) => {
        if (e.target.value === "") setFiltered(missions);
        else setFiltered(missions.filter(item =>  {
            if (item.name) return item.name.toLowerCase().trim().includes(e.target.value.toLowerCase().trim());
            else return false;
        }));
    }

    const gameIdChanged = id => {
        setGameId(id);
    }

    const fetchMissions = async () => {
        try
        {
            const fetched = await getMissions(gameId);
            setMissions(new Array(...fetched));
            setFiltered(new Array(...fetched));
        }
        catch (e)
        {
            setFiltered([]);
            setMissions([]);
        }
    }

    useEffect(() => {
        fetchMissions();
    }, [gameId])

    const editMission = () =>{
        fetchMissions();
    }

    const listItems = () => {
        return filtered.sort((a,b) => a.missionId - b.missionId).map((mission, index) => {
            return (
                <ListGroup.Item className="list-item" action key={index}>
                    {mission.name}
                    <EditMission missionEdited={editMission} mission={mission}/>
                </ListGroup.Item>
            )
        })
    }


    return(
        <div>
            <div className="mission-header">
                <h3>Missions</h3>
                <div className="header-content">
                    <CreateMission missionsEdited={editMission} gameId={gameId} className="btn"/>
                    <SelectGame games={props.games} gameChanged={gameIdChanged}/>
                    <Form.Control className="header-search" type="text" placeholder="Search missions..." onChange={filterChange}/>
                </div>
            </div>
            <div>
                <ListGroup className="list-display">
                    {listItems()}
                </ListGroup>
            </div>
        </div>
    )
}
export default Missions;
