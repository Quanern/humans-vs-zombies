import './GameDetailPage.css'
import Squad from "../Squad/Squad";
import SquadDetail from "../SquadDetail/SquadDetail";
import Chat from "../Chat/Chat";
import GameDetailTitle from "../GameDetailTitle/GameDetailTitle";
import RegistrationFragment from "../RegistrationFragment/RegistrationFragment";
import Map from "../Map/Map";
import BiteCode from "../BiteCode/BiteCode";
import BiteCodeEntry from "../BiteCode/BiteCodeEntry";
import {useEffect, useState} from "react";
import {getPlayers, getSpecificGame} from "../../Services/fetchMethods";
import {useHistory, useLocation} from "react-router-dom";
import UserService from "../../Services/UserService";
import RenderOnRole from "../HOC/RenderOnRole";

function GameDetailPage(){
    const location = useLocation();
    const locationPath = location.pathname.split('/');
    const currentLocation = locationPath[locationPath.length - 1];
    const [player, setPlayer] = useState();
    const [game, setGame] = useState();
    const [role, setRole] = useState("Unregistered");
    const history = useHistory();

    useEffect(() => {
        const getPlayer = async () => {
            return new Promise(async (resolve, reject) => {
                const g = await getSpecificGame(currentLocation).catch(err => {
                    console.log(err)
                    return history.push("/404");
                });
                setGame(g);
                const players = await getPlayers(currentLocation);
                if (!players) return reject([]);
                for (const p of players)
                {
                    if (p.user.username === UserService.getUsername())
                        return resolve(p);
                }
                reject(new Error("Could not find player"))
            })
        }
        getPlayer().then(p => {
            setPlayer(p);
            if (!p)
                setRole("Unregistered");
            else {
                if (p.user.admin === true)
                    setRole("Admin");
                else {
                    if (p.squadMember)
                        setRole("Squad Member");
                    else setRole("Player");
                }
            }
        }).catch(err => {
            console.log(err)
            if (UserService.hasRole(["Admin"]))
                setRole("Admin");
        });
    }, []);

    const renderBiteCode = () => {
        if (!game) return;
        if (game.gameState.stateId === 2 && (role === "Squad Member" || role === "Player")) {
            return(
                <div className="bite-code">
                    {player.human ? <BiteCode biteCode={player.biteCode}/> : <BiteCodeEntry player={player} game={game} />}
                </div>
            );
        }
        else return (
            <div className="bite-code">
            </div>
        )
    }

    const renderRegistrationButton = () =>{
        if (game)
        {
            if (game.gameState.stateId === 1)
                return(
                    <RenderOnRole roles={["User"]}>
                        <RegistrationFragment gameId={currentLocation} username={UserService.getUsername()} hasRegistered={player}/>
                    </RenderOnRole>
                );
        }
    }

    const renderSquadComponents = () => {
        return(
            <div className="squad-detail">
                <Squad player={player} game={game} role={role}/>
                { role === "Squad Member" && <SquadDetail player={player} game={game} role={role}/> }
            </div>
        )
    }

    const renderChat = () => {
        if (role !== "Unregistered")
        {
            return (
                <Chat role={role} gameId = {currentLocation} player = {player}/>
            )
        }
    }

    const renderMap = () => {
        if (game)
        {
            return(
                <Map player={player} role={role} game={game} latSouth={Number(game.south)} latNorth={Number(game.north)} lngWest={Number(game.west)} lngEast={Number(game.east)}/>
            )
        }
    }

    return(
        <div>
            <div className="chat2">
                {renderChat()}
            </div>
            <div className="game-detail-page">
                <div className="game-detail-title">
                    <GameDetailTitle rules='Here is the rules'/>
                    {renderRegistrationButton()}
                </div>
                <div>
                    <div className="game-detail-map">
                        {renderMap()}
                    </div>
                </div>
                <div className="game-detail-content">
                    {renderBiteCode()}
                    {renderSquadComponents()}
                </div>
            </div>
        </div>
    )
}
export default GameDetailPage;
