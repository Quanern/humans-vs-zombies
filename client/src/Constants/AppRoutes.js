export const AppRoutes = {
    LandingPage: '/',
    Login: '/login',
    Profile: '/profile',
    GameDetails: '/game/:gameId',
    AdminPage: '/admin'
}
