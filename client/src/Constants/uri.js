export const URI = {
    ResourceServer: {
        Development: 'http://localhost:8080',
        Production: 'https://hvz-server.herokuapp.com'
    },

    Client: {
        Development: 'http://localhost:3000',
        Production: 'https://hvz-client.herokuapp.com',
    }
}
URI.ResourceServer.Default = URI.ResourceServer.Production
URI.Client.Default = URI.Client.Production
