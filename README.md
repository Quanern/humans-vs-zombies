# Humans vs Zombies
Humans vs Zombies is a case project from Noroff's accelerated learning course. Humans vs Zombies,
from now on abbreviated as hvz, is an administrative tool for the game hvz. Hvz is a game where
there is a patient zero, the first zombie, who's sole purpose is to tag and infect other players.

This project is separated into 3 main parts:
- Client - React Application
- Server - Spring with JPA and Hibernate
- Security - Keycloak and Spring security

This project is also hosted on heroku.
[Client](https://hvz-client.herokuapp.com) and [Server](https://hvz-server.herokuapp.com).

## API Endpoints
All endpoints can be found [here](https://hvz-server.herokuapp.com).

## Installation
Clone or download this project from the repository. 
### Requirements
Client: Node.js, React  
Server: Maven
### Client
Note that the client is now in a production state. To change this, open the uri file found in the Constants folder
and change the default to URI.XXX.Development instead of URI.XXX.Production.
1. Navigate into the client folder with your terminal, or open one in there.
2. Run `npm install` in the terminal.
3. Run `npm start` in the terminal.
4. The browser should now open the client in a new tab.

### Server
1. Open the server as a project in an IDE that supports maven projects.
2. Install all dependencies.
3. You can either run the server through the IDE or through a terminal.

   * **To run the server through the IDE**, just define the configuration as a Spring Boot Server if it isn't already.
       Before running through the IDE, you need to define your 
      **own** database credentials inside [application.properties](server/src/main/resources/application.properties).
      
   * **To run the server from the terminal**, you need to open the maven tab in your IDE first and choose the option *package* to
       get a jar file of the server. This jar file will be generated to a target folder in *server/src*. Navigate to the target folder 
      in your favorite terminal and run the command:   
      `java -jar {JAR_FILENAME}.jar --url={DB_URL} --uname={DB_username} --pw={DB_PASSWORD}`



## Authors
Bhart, Pranav  
Iljasov, Sultan  
Koteng, Markus  
Tran, Quan
